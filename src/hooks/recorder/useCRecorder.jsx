/*
* WebRTC录音
*/
import { useEffect, useRef } from 'react'
import { Toast } from 'antd-mobile';
import dayjs from "dayjs";

import MediaRecorder from 'opus-media-recorder';
// eslint-disable-next-line import/no-webpack-loader-syntax
import EncoderWorker from 'worker-loader!opus-media-recorder/encoderWorker.js';

// Non-standard options
const workerOptions = {
  encoderWorkerFactory: _ => new EncoderWorker(),
};

let speakStartTime = 0;
let recorder = {};
let recordRequestId = null;
let audioSequence = 0;

export default function (onCreateMediaSuccess, onCreateMediaFail) {
  const mediaStream = useRef(null);

  const closeStream = () => {
    if (mediaStream.current && mediaStream.current.getTracks) {
      mediaStream.current.getTracks().forEach(track => {
        track.stop();
      });
      mediaStream.current = null;
    }
  };

  const sendAudio = (wavBlob, sequence) => {
    function transformArrayBufferToBase64(buffer) {
      let binary = '';
      const bytes = new Uint8Array(buffer);
      for (let len = bytes.byteLength, i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
      }
      return window.btoa(binary);
    }

    const reader = new FileReader();
    reader.addEventListener('load', async function (evt) {
      const buffer = evt.target.result;
      const bWav = transformArrayBufferToBase64(buffer);

      // 仅用于开发自测
      // if (bWav.length > 58) {
      //   const findEmptyStr = bWav.slice(58).split('').filter(el => el !== 'A').filter(el => el !== '=');
      //   const isEmptyVoice = findEmptyStr.length === 0;
      //   console.log('是否是空音频',isEmptyVoice);
      // }

      window.sendData({
        requestId: recordRequestId,
        sequence,
        type: 'audio',
        format: 'wav',
        audio: bWav
      });
    });
    reader.readAsArrayBuffer(wavBlob);
  }

  const createRecorder = () => {
    navigator.mediaDevices.getUserMedia({audio: {
      sampleRate: 16000,        // 采样率
      channelCount: 1,          // 声道
      audioBitsPerSecond : 16,  // 输出采样数位 8, 16;
      volume: 1.0,              // 音量
      autoGainControl: true
    }})
    .then(stream => {
      mediaStream.current = stream;
      recorder = new MediaRecorder(stream, { mimeType: 'audio/wav' }, workerOptions);
      recorder.addEventListener('dataavailable', (e) => {
        audioSequence ++;
        sendAudio(e.data, audioSequence);
      });
      onCreateMediaSuccess();
      window.sendData({
        "version": "1.0",
        "action_type": "log",
        "sync": true,
        "extra": {
          "EventType": "AuthRecorderSuccess",
          "WebsocketID": window.$gVar.sessionId,
          "dialog_node_id": window.$gVar.dialog_node_id,
          "RequestID": window.$gVar.requestId,
          "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
        }
      });
    })
    .catch(err => {
      Toast.fail(`开启录音失败，请刷新重试`);
      onCreateMediaFail();
      console.error(err);
      window.sendData && window.sendData({
        "version": "1.0",
        "action_type": "log",
        "sync": true,
        "extra": {
          "EventType": "AuthRecorderFail",
          "WebsocketID": window.$gVar.sessionId,
          "dialog_node_id": window.$gVar.dialog_node_id,
          "RequestID": window.$gVar.requestId,
          "ErrorName": err.name,
          "ErrorMsg": err.message,
          "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
        }
      });
    });
  }

  const startRecord = e => {
    recordRequestId = Date.parse(new Date());
    window.$gVar.recordRequestId = recordRequestId;
    audioSequence = 0;
    recorder.start(300);  // 300ms 一个片段
    speakStartTime = new Date().getTime();
  };

  const stopRecord = () => {
    const speakTime = new Date().getTime() - speakStartTime;
    if(speakTime < 1000) {
      Toast.fail('请长按作答');
      recorder.stop();
      return;
    }

    setTimeout(() => {
      recorder.stop();
      setTimeout(() => {
        window.sendData({
          requestId: recordRequestId,
          type: 'audio_interrupt'
        });
      }, 500);
    }, 300);   // 多录300ms
  };

  useEffect(() => {
    createRecorder();
    return () => {
      closeStream();
      recorder = null;
      recordRequestId = null;
      audioSequence = null;
    };
    /* eslint-disable-next-line */
  }, []);

  return {
    startRecord,
    stopRecord
  };
}
