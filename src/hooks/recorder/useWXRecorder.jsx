/*
* 微信录音
*/
import { useEffect } from 'react'
import { Toast } from 'antd-mobile'
import dayjs from "dayjs";

let speakStartTime = 0;
let recordRequestId = null;
let recodLocalId = null;

export default (onCreateMediaSuccess, onCreateMediaFail, handleRecordEndBusiness) => {

  const uploadAudio = localId => {
    window.wx.uploadVoice({
      localId,
      isShowProgressTips: 0,            // 默认为1，显示进度提示
      success(res) {
        const serverId = res.serverId;    // 返回音频的服务器端ID
        console.log('微信录音音频上传成功, serverId: ', serverId);
        window.sendData({
          format: 'amr',
          type: 'wx_audio',
          mediaId: serverId,
          wxId: 'pufa01',   // 浦发银行: pufa01,  三方：'anluojie'
          requestId: recordRequestId
        });

        window.sendData({
          "version": "1.0",
          "action_type": "log",
          "sync": true,
          "extra": {
            "EventType": "AuthRecorderSuccess",
            "WebsocketID": window.$gVar.sessionId,
            "dialog_node_id": window.$gVar.dialog_node_id,
            "RequestID": window.$gVar.requestId,
            "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
          }
        });
      }
    });
  }

  useEffect(() => {
    // 录音时间超过一分钟没有停止的时候会执行 complete 回调
    window.wx.onVoiceRecordEnd({
      complete(res) {
        console.log('onRecordEnded res: ', res);
        const localId = res.localId;
        uploadAudio(localId);
        handleRecordEndBusiness();
      }
    });
    /* eslint-disable-next-line */
  }, []);

  const startRecord = e => {
    recordRequestId = Date.parse(new Date());
    window.$gVar.recordRequestId = recordRequestId;
    
    window.wx.startRecord();
    speakStartTime = new Date().getTime();
  };

  const stopRecord = () => {
    const speakTime = new Date().getTime() - speakStartTime;
    
    if(speakTime < 1000){
      Toast.fail(`请长按按钮作答`);
      window.wx.stopRecord();
      return;
    }

    window.wx.stopRecord({
      success(res) {
        console.log('stop success: ', res);
        recodLocalId = res.localId;
        window.$gVar.recodLocalId = res.localId;
        const localId = res.localId;
        uploadAudio(localId);
      },
      fail(res) {
        Toast.fail('录音失败');
        console.error(res);
        // tag: log AuthRecorderFail事件
        window.sendData({
          "version": "1.0",
          "action_type": "log",
          "sync": true,
          "extra": {
            "EventType": "AuthRecorderFail",
            "WebsocketID": window.$gVar.sessionId,
            "dialog_node_id": window.$gVar.dialog_node_id,
            "RequestID": window.$gVar.requestId,
            "ErrorName": res.name,
            "ErrorMsg": res.message,
            "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
          }
        });
      }
    });
  };

  const playVoice = () => {
    window.wx.playVoice({
      localId: recodLocalId, // 需要播放的音频的本地ID，由stopRecord接口获得
      success:function(){
          console.log('播放成功');
      },
      fail:function() {
          console.log('失败');
      }
    });
  }

  const pauseVoice = () => {
    window.wx.pauseVoice({
      localId: recodLocalId // 需要暂停的音频的本地ID，由stopRecord接口获得
    });
  } 

  const translateVoice = () => {
    window.wx.translateVoice({
      localId: recodLocalId, // 需要识别的音频的本地Id，由录音相关接口获得
      isShowProgressTips: 1, // 默认为1，显示进度提示
      success: function (res) {
        console.log("userWXRecoder-语音识别结果",res.translateResult); // 语音识别的结果
      }
    });
  }

  const onVoicePlayEnd = () => {
  //监听语音播完结束
    window.wx.onVoicePlayEnd({
      success: function (res) {
        var localId = recodLocalId; // 返回音频的本地ID
        console.log("语音播放完毕",res)
      }
    });  
  }

  return {
    startRecord,
    stopRecord,
    playVoice,
    pauseVoice,
    translateVoice,
    onVoicePlayEnd
  };
}
