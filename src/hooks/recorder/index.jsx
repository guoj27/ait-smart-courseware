import useCRecorder from './useCRecorder';
import useWXRecorder from './useWXRecorder';

export default isWXReady => {
  if (isWXReady) {
    return useWXRecorder;
  }
  return useCRecorder;
}