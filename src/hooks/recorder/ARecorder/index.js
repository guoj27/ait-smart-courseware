/*
* 使用 AudioContext 处理音频
*/
import React, { useState, useEffect, useRef } from 'react';
import { CSSTransition } from "react-transition-group";
import { Button, Toast } from 'antd-mobile';
import dayjs from "dayjs";

import { wait } from 'util';
import useInterval from 'hooks/useInterval';
import Recorder from './Recorder';
import '../style.scss'

let speakStartTime = 0;
let recordRequestId = null;
let audioSequence = 0;
let isError = false;

export default function (props) {
  const [isTouchEnd, setIsTouchEnd] = useState(false);
  const [isMaskShow, setIsMaskShow] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const [duration, setDuration] = useState(null);

  const recorder = useRef(null);
  const mediaStream = useRef(null);

  useEffect(() => {
    setIsShow(props.isShow);
  },[props.isShow]);

  useEffect(() => {
    return () => {
      recorder.current && recorder.current.closeAudioContext();
      if (mediaStream.current) {
        mediaStream.current.getTracks().forEach(track => {
          track.stop();
        });
      }
    }
  }, []);

  useInterval(() => {
    if (props.recordLeftTime <= 0) {
      setDuration(null);
      return;
    }
    props.updateRecordLeftTime(-10);
  }, duration);

  const sendAudio = (wavBlob, sequence) => {
    function transformArrayBufferToBase64(buffer) {
      let binary = '';
      const bytes = new Uint8Array(buffer);
      for (let len = bytes.byteLength, i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
      }
      return window.btoa(binary);
    }
    
    const reader = new FileReader();
    reader.addEventListener("load", async function (evt) {
      const bWav = transformArrayBufferToBase64(evt.target.result);
      
      if(bWav.length > 58){
        const findEmptyStr = bWav.slice(58).split('').filter(el => el !== 'A').filter(el => el !== '=');
        const isEmptyVoice = findEmptyStr.length === 0;
        console.log('是否是空音频',isEmptyVoice);
        console.log('音频前10位',findEmptyStr.slice(0,10).join(''));
      }

      // await wait(200);   // 待验证
      window.sendData({
        requestId: recordRequestId,
        sequence,
        type: 'audio',
        format: 'wav',
        audio: bWav
      });
    });
    reader.readAsArrayBuffer(wavBlob);
  }

  const datavaliable = blob => {
    audioSequence ++;
    sendAudio(blob, audioSequence);
  }

  useEffect(() => {
    navigator.mediaDevices.getUserMedia({audio: {
      sampleRate: 16000,        // 采样率
      channelCount: 1,          // 声道
      audioBitsPerSecond : 16,  // 输出采样数位 8, 16;
      volume: 1.0,              // 音量
      autoGainControl: true
    }})
    .then(stream => {
      mediaStream.current = stream;
      isError = false;
      // tag: log AuthRecorderSuccess事件
      window.sendData({
        "version": "1.0",
        "action_type": "log",
        "sync": true,
        "extra": {
          "EventType": "AuthRecorderSuccess",
          "WebsocketID": window.$gVar.sessionId,
          "dialog_node_id": window.$gVar.dialog_node_id,
          "RequestID": window.$gVar.requestId,
          "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
        }
      });
    })
    .catch(err => {
      isError = true;
      Toast.fail(`开启录音失败，请刷新重试`);
      console.error(err);
      // tag: log AuthRecorderFail事件
      window.sendData({
        "version": "1.0",
        "action_type": "log",
        "sync": true,
        "extra": {
          "EventType": "AuthRecorderFail",
          "WebsocketID": window.$gVar.sessionId,
          "dialog_node_id": window.$gVar.dialog_node_id,
          "RequestID": window.$gVar.requestId,
          "ErrorName": err.name,
          "ErrorMsg": err.message,
          "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
        }
      });
    });
  }, []);

  const startRecord = e => {
    recorder.current && recorder.current.closeAudioContext();
    recorder.current = new Recorder(mediaStream.current, {}, datavaliable);
    recorder.current.start();

    setIsMaskShow(true);
    setDuration(10000);  // 10s

    recordRequestId = Date.parse(new Date());
    window.$gVar.recordRequestId = recordRequestId;
    audioSequence = 0;
    
    props.onStart(true);
    speakStartTime = new Date().getTime();
    
    setIsTouchEnd(true);
  };

  const stopRecord = async() => {
    setIsMaskShow(false);
    setDuration(null);
    setIsTouchEnd(false);

    props.onStart(false);
    const speakTime = new Date().getTime() - speakStartTime;

    if(speakTime < 200){
      Toast.fail(`您放得太快了，请试着按久一些`);
      recorder.current.stop();
      return;
    }
    
    props.onEnded();

    await wait(300);     // 多录1s, 否则吃字
    recorder.current.stop();

    if (isError) {
      return;
    }

    await wait(200);
    window.sendData({
      requestId: recordRequestId,
      type: 'audio_interrupt'
    });

    download();
  };

  const download = () => {
    // if(recorder.current === null) {
    //   Toast.fail('请先录音');
    //   return;
    // }
    // const src = recorder.current.wavSrc();
    // const a = document.createElement('a');
    // a.href = src;
    // a.download = 'ar';
    // a.click();
  };

  return (
    <>
      <div className={`recorder-mask ${isMaskShow ? 'show':''}`}>
        <div className="text">
          长按并回答<br />
          说完后松开
        </div>
      </div>

      <CSSTransition in={isShow} timeout={300} classNames='fadeIn'>
        <div className="recorder-wrap">
          <Button
            className={`btn ${isTouchEnd ? '' : 'flash-ani'}`}
            onTouchStart={startRecord}
            onTouchEnd={stopRecord}
          >
            {/*兼容安卓浏览器长按出现下载*/}
            <span className="bg"/>
          </Button>
          <div className="tip">
            {isTouchEnd ? '':'按住 说话'}
          </div>
          {
            isTouchEnd ? (
                <div className="halo-wrap">
                  <div className="halo item-1"></div>
                  <div className="halo item-2"></div>
                  <div className="halo item-3"></div>
                </div>
            ) : null
          }
        </div>
      </CSSTransition>
    </>
  );
}
