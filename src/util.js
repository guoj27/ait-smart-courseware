import UAParse from 'ua-parser-js';
import CryptoJS from 'crypto-js/crypto-js'

/* eslint-disable no-undef */
export const publicPath =  PUBLIC_PATH;

/* eslint-disable no-undef */
export const deployEnv = DEPLOY_ENV;

const ua_parse = new UAParse();
const uaRes = ua_parse.getResult();
export const isAndroid = uaRes.os.name === 'Android';
export const isIOS = uaRes.os.name === 'iOS';
export const isChrome = uaRes.browser.name.includes('Chrome');
export const isSafari = uaRes.browser.name.includes('Safari');
export const isWeChat = uaRes.browser.name === 'WeChat';

export const getBaseUrl = () => {
  const uatRestfulBaseUrl = 'https://dh.spdb-xlab.cn:4430';
  const baseUrl = {
    dev: uatRestfulBaseUrl,
    sit: uatRestfulBaseUrl,
    uat: uatRestfulBaseUrl,
    wx: uatRestfulBaseUrl,
    prod: ''
  };
  return baseUrl[deployEnv];
}
export const getUUID = () => {
  const s4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  return (s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4());
};
export const getWebsocketUrl = (token) => {
  const devUrl = 'wss://dh.spdb-xlab.cn:4430/digital-human-platform-dev-web-socket?token=' + token;
  const prodUrl = 'wss://x.spdb.com.cn/digital-human-platform-mkt-web-socket?token='+ token;
  const websocketUrl = {
    dev: devUrl,
    sit: devUrl,
    uat: devUrl,
    wx: devUrl,
    prod: prodUrl
  }
  return websocketUrl[deployEnv];
}

export const reload = () => {
  if(window.wx && wx.miniProgram){
    wx.miniProgram.getEnv(function (res) {
      if (res.miniprogram) {
        wx.miniProgram.navigateTo({
          url: '/pages/authorization/authorization',//跳转回小程序的页面,传参id, 小程序onLoad函数接收参数即可
          success: function () {
            console.log('success')
          },
         fail: function () {
          console.log('跳转回小程序的页面fail');
         },
       });
      } else {
        window.location.href = window.location.origin + publicPath;
      }
    });
  }else {
    window.location.href = window.location.origin + publicPath;
  }
  
}

/* 工具类方法 */
export const wait = time => new Promise(resolve => setTimeout(resolve, time));

/* 工具类方法 */
export const transAicpList = listObj => {
  if (!(listObj && typeof listObj === 'object')) {
    return [];
  }
  const keys = Object.keys(listObj);
  const likeArray = { length: keys.length };
  keys.forEach(key => {
    likeArray[key.replace('list_', '')] = listObj[key];
  });
  return Array.from(likeArray);
}

export const transTime = dateTimeStamp => {
  const minute = 60 * 1000;
	const hour = minute * 60;
	const day = hour * 24;
	// const halfamonth = day * 15;
  const month = day * 30;
  
	const now = new Date().getTime();
	const diffValue = now - dateTimeStamp;
  if (diffValue < 0) return;
  
	const monthC = diffValue / month;
	const weekC = diffValue / (7 * day);
	const dayC = diffValue / day;
	const hourC = diffValue / hour;
  const minC = diffValue / minute;

  let result = null;
  
	if(monthC >= 1){
		result = "" + parseInt(monthC) + "月前";
	} else if (weekC >= 1){
		result = "" + parseInt(weekC) + "周前";
	} else if (dayC >= 1){
		result = "" + parseInt(dayC) +"天前";
	} else if(hourC>=1){
		result=""+ parseInt(hourC) +"小时前";
	} else if(minC>=1){
		result=""+ parseInt(minC) +"分钟前";
	} else {
    result="刚刚";
  }
  return result;
}

export const getUrlParam = () => {
    const paramStr = window.location.href.split('?')[1];
    if (!paramStr) {
        return {};
    }
    const paramMaps = paramStr.split('&');
    const param = {};
    paramMaps.forEach(item => {
        const arr = item.split('=');
        param[arr[0]] = arr[1];
    });
    return param;
}

const KEY = CryptoJS.enc.Utf8.parse("1234567890123456");
export const encrypt = (word) => {
  
  let srcs = CryptoJS.enc.Utf8.parse(word);
  let encrypted = CryptoJS.AES.encrypt(srcs, KEY, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
  
}

export const objectToArr = (obj) => {
  const arr = [];
  if(Array.isArray(obj)){
    return obj;
  }else{
    arr.push(obj)
    return arr
    
  }
}

export const isMiniPro = () => {
  if(window.wx && wx.miniProgram){
    wx.miniProgram.getEnv(function (res) {
      return res.miniProgram;
    });
  }else {
    reload();
  }
}