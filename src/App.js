import React from 'react';
import { Switch, Route, HashRouter } from "react-router-dom";

import Redirect from 'pages/Redirect';
import Login from 'pages/Login';
import Entry from 'pages/Entry';
import ErrorPage from 'pages/Error';
import Documents from 'widget/Documents';

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route exact path='/' component={Redirect} />
        <Route path='/login' component={Login} /> 
        <Route path='/entry' component={Entry} />
        <Route path='/doc' component={Documents} />
        <Route path='/error/:msg' component={ErrorPage} />
        {/* for test */}
        
      </Switch>
    </HashRouter>
  );
}

export default App;
