import React, { useEffect, useState } from 'react';
import { Button, InputItem, Toast } from "antd-mobile";
import { CSSTransition } from "react-transition-group";
import NoSleep from 'nosleep.js';

import useInterval from 'hooks/useInterval'
import ErrorModal from 'components/ErrorModal';

import api from "axios/api";
import { isAndroid, isIOS, isChrome, isSafari, isWeChat, getUrlParam, encrypt, deployEnv, getUUID } from 'util';

import './style.scss';

const cacheImgs = [
  require('static/glass-bg.jpg'),
  require('static/mask_bg.png'),
  require('static/topics/gold-medal.png'),
  require('static/topics/silver-medal.png')

];
// 预留测试getUserMedia接口
const DEFAULT_LOADING_ITEMS = 1 + cacheImgs.length;

// 防止手机休眠, 视频衔接改成图片，容易导致移动端手机休眠
const noSleep = new NoSleep();

let cacheVideoNum = 0;
let valid = true;
let getCountTimer = null;

export default function (props) {
  const [errorType, setErrorType] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isEntering, setIsEntering] = useState(false);
  const [loadingNum, setLoadingNum] = useState(0);
  const [telValue, setTelValue] = useState(localStorage.getItem("tel") || "");
  const [idValue, setIdValue] = useState(localStorage.getItem("employeedId") || "");
  const [checkCode, setCheckCode] = useState("");

  const {history} = props;
  // const idleVideos = window.$gVar.idleVideos || [];
  const [sceneCount, setSceneCount] = useState(0);
  const [allCount, setAllCount] = useState(0);
  const [onlineUser, setOnlineUser] = useState(0);
  const [loginButton, setLoginButton] = useState(false);
  const [duration, setDuration] = useState(30);

  const base64ImgPrefix = "data:image/png;base64,";
  const [imageUrl, setImageUrl] = useState("");
  const [sessionId, setSessionId] = useState("");

  const isDev = deployEnv === 'dev';

  useEffect(() => {
    getImgcode();
    return () => {
      setDuration(null);
      clearInterval(getCountTimer);
      getCountTimer = null;
    }
  /* eslint-disable-next-line */
  }, []);

  const enterAction = () => {
    setIsLoading(false);
    setTimeout(() => setIsEntering(true), 500);
     
  };

  useInterval(() => {
    if (loadingNum < 100) {
      if (!(loadingNum > 94 && cacheVideoNum < DEFAULT_LOADING_ITEMS)) {
        setLoadingNum(loadingNum + 1);
      }
      if (loadingNum === 99) {
        enterAction();
      }
    } else {
      setDuration(null);
    }
  }, duration);

  useEffect(() => {
    window.$gVar.isWeChat = isWeChat;
    console.log("是否是微信浏览器： ", isWeChat);

    const checkBrowser = () => {
      if (isIOS && !(isSafari || isWeChat)) {
        setErrorType(1);
        valid = false;
      }

      if (isAndroid && !(isChrome || isWeChat)) {
        window.$gVar.isSupCanvasDrawVideo = false;
        setErrorType(2);
        valid = false;
      }
    }
    checkBrowser();

    // getUserMedia 版本兼容
    const initUserMedia = () => {
      if (navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};
      }

      if (navigator.mediaDevices.getUserMedia === undefined) {
        navigator.mediaDevices.getUserMedia = function (constraints) {
          let getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

          if (!getUserMedia) {
            return Promise.reject(new Error('浏览器不支持 getUserMedia !'));
          }

          return new Promise(function (resolve, reject) {
            getUserMedia.call(navigator, constraints, resolve, reject);
          });
        }
      }
    };

    !isWeChat && initUserMedia();

    const checkGetUserMedia = async function () {
      window.$gVar.isSupRecorder = true;

      if (isWeChat) {
        cacheVideoNum++;
        return;
      }
      
      try {
        const stream = await navigator.mediaDevices.getUserMedia({audio: true, video: false});
        stream.getTracks().forEach(track => track.stop());
      } catch (e) {
        console.error(`不支持录音`, e);
        window.$gVar.isSupRecorder = false;
        valid = false;

        // 没有权限
        if (e.name === 'NotAllowedError') {
          setErrorType(3);
        } else {
          setErrorType(4);
        }
      }
      cacheVideoNum++;

      try {
        window.getRenderer(function(value) {
          if (value === 'Unknown') {
            const canvas = document.createElement("canvas");
            if (canvas != null) {
              const context = canvas.getContext("webgl") ||
                  canvas.getContext("experimental-webgl");
              if (context) {
                const info = context.getExtension("WEBGL_debug_renderer_info");
                if (info) {
                  value = context.getParameter(info.UNMASKED_RENDERER_WEBGL);
                }
              }
            }
          }
          window.$gVar.GPUInfo = value;
          console.log('GPU-info', value);
        });
      } catch (e) {
        console.error(`不支持gpu检测，`, e.name,e.message)
      }
    };

    valid && checkGetUserMedia();

    // const checkCanvasDrawVideo = function (video) {
    //   const canvasEle = document.createElement('canvas');
    //   canvasEle.width = 10;
    //   canvasEle.height = 10;
    //   const ctx = canvasEle.getContext('2d');
    //   try {
    //     ctx.drawImage(video, 0, 0, 10, 10);
    //     window.$gVar.isSupCanvasDrawVideo = true
    //   } catch (e) {
    //     console.error(`不支持video绘制到canvas`, e.message);
    //     window.$gVar.isSupCanvasDrawVideo = false;
    //     valid = false;
    //     setErrorType(5);
    //   }
    //   cacheVideoNum++;
    // };

    // if (valid) {
    //   for (let i = 0; i < idleVideos.length; i++) {
    //     const idleVideo = idleVideos[i];
    //     let videoEle = document.createElement('video');
    //     videoEle.autoplay = true;
    //     videoEle.playsinline = true;
    //     videoEle.muted = true;
    //     videoEle.hidden = true;
    //     videoEle.src = window.$gVar.cdnPath + idleVideo;
    //     videoEle.preload = 'auto';
    //     // eslint-disable-next-line no-loop-func
    //     videoEle.addEventListener('canplaythrough', ev => {
    //       if (i === 0) {
    //         checkCanvasDrawVideo(videoEle);
    //       }
    //       cacheVideoNum++;
    //     });
    //     videoEle.load();
    //   }
    // }

    for (let i = 0; i < cacheImgs.length; i++) {
      const cacheImg = cacheImgs[i];
      const imgELe = document.createElement('img');
      imgELe.src = cacheImg;
      // eslint-disable-next-line no-loop-func
      imgELe.onload = () => {
        cacheVideoNum++;
      };
    }

    setTimeout(() => {
      if (cacheVideoNum !== DEFAULT_LOADING_ITEMS) {
        cacheVideoNum = DEFAULT_LOADING_ITEMS;
      }
    }, 6000); // 6秒超时就直接加载完成

    // 当前人数接口调用
    const getCount = () => {
      api.getCount(getUrlParam().AID || window.$gVar.AID).then(resData => {
        if(resData) {
          setAllCount(resData.allCount);
          setSceneCount(resData.sceneCount);
          setOnlineUser(resData.onlineUser);
        }
      });
    }
    getCount()
    getCountTimer = setInterval(() => {
      getCount();
    }, 20000);
    // eslint-disable-next-line
  }, []);

  const modalContent = errType => {
    let str = '';
    switch (errType) {
      case 1:
        str = '请使用微信浏览器或Safari浏览器打开该网页';
        break;
      case 2:
        str = '请使用微信浏览器或Chrome浏览器打开该网页';
        break;
      case 3:
        str = '完成培训需要开启语音权限，请刷新页面后开启权限';
        break;
      case 4:
        str = '很抱歉当前不支持录音';
        break;
      case 5:
        str = '浏览器不支持视频绘制';
        break;
      default:
        str = ''
    }
    return str
  };

  const telChange = value => {
    setTelValue(value)
  };

  const idChange = value => {
    setIdValue(value)
  };

  const codeChange = value => {
    setCheckCode(value);
  }

  const login = () => {
    // 去除中间和头尾的空格
    const id = idValue.replace(/\s*/g, '').replace(/^\s*|\s*$/g, '');
    const tel = telValue.replace(/\s*/g, '').replace(/^\s*|\s*$/g, '');

    if (!id) {
      Toast.fail('工号不能为空', 1);
      return;
    }
    
    if (!tel) {
      Toast.fail('手机号不能为空', 1);
      return;
    }

    if (!isDev && !checkCode) {
      Toast.fail('验证码不能为空', 1);
      return;
    }

    noSleep.enable();
    window.$gVar.noSleep = noSleep;


    setLoginButton(true);
    const values = {
      sessionId,
      checkCode,
      employeeId: encrypt(id),
      mobile: encrypt(tel),
      refId:getUrlParam().refId,
      aid:window.$gVar.AID 
    }
    const fetchFn = isDev ? api.devLogin : api.loginWithCode;
    fetchFn(values).then(resData => {
      if (resData) {
        sessionStorage.setItem("token", resData);
        clearInterval(getCountTimer);
        const refId = getUrlParam().refId;
        if (refId) {
          sessionStorage.setItem("refId", refId);
        }
        window.$gVar.employeeId = id;
        sessionStorage.setItem("employeeId", id);
        localStorage.setItem("employeedId", idValue);
        localStorage.setItem("tel", telValue);
        localStorage.setItem("connectSessionId", getUUID());
        history.push(`/entry`);
      } else {
        getImgcode();
        setLoginButton(false);
      }
    })
  }

  const getImgcode = () => {
    
    setCheckCode("");
    api.getImgcode().then(resData => {
      setImageUrl(resData.imageBase64);
      setSessionId(resData.sessionId);
    })
  }

  return (
      <div className="start-wrap">
        <ErrorModal isShow={errorType} content={modalContent(errorType)} />

        <CSSTransition in={isLoading} classNames='fade' timeout={500}>
          <div className="loading-wrap">
            <div className="mask"/>
            <div className="text">
              <div>{loadingNum}%</div>
              <div className="loading-text">Loading...</div>
              <div className={`halo`}/>
            </div>
          </div>
        </CSSTransition>

        <CSSTransition in={isEntering && !errorType} classNames='fade' timeout={1000}>
          <div className="enter-wrap">
            <div className="login-bg">
              <div className="login-banner">
              </div>
                <div className="content">
                <div className="title"> 私募雪球 第 <span style={{color:window.fundcolor}}>{window.fundcount}</span> 期</div>
                  <InputItem
                    className="login-input num-input"
                    placeholder='工号'
                    value = {idValue}
                    onChange = {idChange}
                  />
                  <InputItem
                    type="phone"
                    className="login-input tel-input"
                    placeholder="手机号"
                    value = {telValue}
                    onChange = {telChange}
                  />
                  <div className="form-item">
                    <InputItem
                      className="login-input code-input"
                      placeholder="验证码"
                      value = {checkCode}
                      onChange = {codeChange}
                    />
                    <span
                      data-stand-id="captcha_refresh"
                      className="captcha-img"
                      onClick={getImgcode}
                    >
                      {imageUrl && <img alt="加载失败" src={`${base64ImgPrefix}${imageUrl}`} />}
                    </span>
                  </div>

                  <div className="countContent">
                    <div>
                      累计参与<span className="count" style={{color:window.fundcolor}}><br/>  {allCount} </span>人
                    </div>
                    <div>
                      实时在线<span className="count" style={{color:window.fundcolor}}><br/> {onlineUser} </span>人
                    </div>
                    <div>
                      本期完成<span className="count" style={{color:window.fundcolor}}><br/>  {sceneCount} </span>人
                    </div>
                  </div>
                </div>

                <div className="bottom-action">
                  <Button
                    type="primary"
                    className="login-btn"
                    loading={loginButton}
                    onClick={login}
                    style={{color:window.fundcolor}}
                  >
                      登录
                  </Button>
                  <div className="prompt-tip">提示：本流程暂不支持使用耳机</div>
                </div>
            </div>
          </div>
        </CSSTransition>
      </div>
  );
}
