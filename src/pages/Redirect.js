/*
* 链接后加上时间戳参数，解决微信的缓存问题
*/

import { publicPath, getUrlParam } from 'util';

export default props => {

  // const AID = getUrlParam().AID || sessionStorage.getItem("AID") || 'AIBOSTraining001';
  const { origin } = window.location;
  // const redirectUrl = origin + publicPath + '#login?v=' + new Date().getTime() + '&AID=' + AID;
  const redirectUrl = origin + publicPath + '#login?v=' + new Date().getTime();
  window.location.href = redirectUrl;
  // window.$gVar.AID = getUrlParam().AID || 'AIBOSTraining001'; // AIMiniTraining003 AIBOSTraining002
  // sessionStorage.setItem('AID', getUrlParam().AID);
  
  return null;
}
