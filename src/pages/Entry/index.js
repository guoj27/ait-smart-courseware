import React from 'react';
import { Toast } from 'antd-mobile';
import _ from 'lodash';

import StompCon from 'pages/Entry/StompCon';
import MainTitle from './MainTitle';
import Video from './Video/NewVideo';
import Recorder from "pages/Entry/BusinessRecorder";
import Widget from "widget";
import Popup from './Popup';
import ErrorModal from 'components/ErrorModal';

import { wait, reload, publicPath, getUrlParam, getUUID } from 'util';
import api from 'axios/api';

import "./style.scss";

let speakTimeoutTimer = null;
let speakTimerTimeout = 0;
let speakTimerPause = true;
let asrTimeout = null;
let vdieoPlayTimer = null;
let speakStartTime = 0;

const isDev = process.env.NODE_ENV === "development";
const recordMaxTime = 60;   // 录音时间限制在60s

export default class Entry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSupRecorder: window.$gVar.isSupRecorder,
      isWXReady: false,
      isCloseWs: true,
      isErrorModalShow: false,
      errorModalContext: '',
      errorModalTitle: '',
      mainTitleText: '',
      isRecorderShow: false,
      isRecorderMount: false,
      subtitles: [],
      curSubtitle: '',
      currentVideo: null,
      preFetches: null,
      branch: {},
      aitQActionShow: false,          // 场景演练答案提交页是否展示
      videoQueEnded: false,
      recordLeftTime: recordMaxTime,   // 默认60秒
      asrTime: 0,
      isRecording: false,
      showSkip: false,
      popupData: null,
      isStep:false,
      Step:false,
      durationTime:0,
      isVoiceEnd:false,
    };
  }

  componentDidMount() {
    if(getUrlParam().token){
      sessionStorage.setItem("token",  getUrlParam().token);
      window.ismini = true
    }else{
      window.ismini = false
    }
    
    localStorage.setItem("connectSessionId", getUUID());
    // if (!(window.$gVar && window.$gVar.AID)) {
    //   reload();
    //   return;
    // }

    // const employeeId = sessionStorage.getItem("employeeId");
    // if(!employeeId) {
    //   reload();
    // }

    this.configWX();
    this.speakTimeoutSet();

    if (isDev) {
      // this.mock();
    }
  }

  componentWillUnmount() {
    window.$gVar.noSleep && window.$gVar.noSleep.disable();

    clearInterval(speakTimeoutTimer);
    clearTimeout(asrTimeout);
    window.$gVar = {};
    speakTimeoutTimer = null;
    speakTimerTimeout = null;
    speakTimerPause = null;
    asrTimeout = null;
    sessionStorage.removeItem("employeeId");
    sessionStorage.removeItem("refId");
  }

  mock = async() => {
  
    // this.videoAction({
    //   title:"嘉实浦惠基金自成立以来，截至2020年11月27日，净值达1.0076元，成立以来收益率0.76%，年化收益率6.33%，期间最大嘉实浦惠基金自成立以来，截至2020年11月27日，净值达1.0076元，成立以来收益率0.76%，年嘉实浦惠基金自成立以来，截至2020年11月27日，净值达1.0076元，成嘉实浦惠基金自成立以来，截至2020年11月27日，净值达1.0076元，成立以来收益率0.76%，年化收益率6.33%，期间最大回撤0.05%立以来收益率0.76%，年化收益率6.33%，期间最大回撤0.05%化收益率6.33%，期间最大回撤0.05%回撤0.05%。从目前来看，较好地抓住了一些市场机会，回避了一些出现风险的品种，初步实现了稳中求进的目标。,嘉实浦惠基金自成立以来，截至2020年11月27日，净值达1.0076元，成立以来收益率0.76%，年化收益率6.33%，期间最大回撤0.05%",
    //   couldSkip: 'true',//下一步
    //   isStep:'true',
    // })
    
    // this.popupAction({
    //     'title': '提醒',
    //     'body': '您上次有未完成的培训呢',
    //     'confirmText': '继续',
    //     'cancelText': '重新开始'
    // })
     this.branchAction({
        data: {
          num: "1",
         title:"完美！你做得真棒！完美！你做得真棒！完美！你做得真棒！"  ,
         score:"60",
         standardChoice: "A",
         titlePicName: "2-q1",//题目有图片才有
         picList: [
           {num:"A", picName:"1-a1", rate: "20%"},
           {num:"B", picName:"1-a2", rate: "40%"},
           {num:"C", picName:"1-a3", rate: "40%"},
           {num:"D", picName:"1-a4", rate: "80%"}
         ]
        //  picList: [
        //   {num:"A", picName:"1-a1"},
        //   {num:"B", picName:"1-a2"},
        //   {num:"C", picName:"1-a3"},
        //   {num:"D", picName:"1-a4"}
        // ]
        },
         
        // type: "ait-total-score",
        // type: "ait-database",
        // type: 'ait-knowledgebase-index',
        type: 'ait-question-index',
        isTrue: "",
      })
  }

  // 注入微信权限验证配置
  configWX = async() => {
    // 如何不是微信浏览器
    if (!window.$gVar.isWeChat) {
      this.setState({ isCloseWs: false });  // 前端mock时，改成true
      return;
    }

    const _this = this;

    const fetchParams = await api.getPufaWXConfig()   // getWXConfig | getPufaWXConfig
      .then(data => data)
      .catch(err => {
        _this.wsError(err || '获取微信签名失败');
      });
    
    if (!fetchParams) {
      return;
    }

    let configSuccess = true;

    const configParams = {
        appId: fetchParams.appId,
        jsApiList: ['startRecord', 'stopRecord', 'onVoiceRecordEnd', 'uploadVoice', 'updateAppMessageShareData', 'playVoice', 'pauseVoice'],
        nonceStr: fetchParams.nonceStr,
        signature: fetchParams.signature,
        timestamp: fetchParams.timestamp,
        debug: false
    };

    window.wx.config(configParams);

    window.wx.error(res => {
      console.log('微信配置信息验证失败：', res);
      configSuccess = false;
      // 生产上打开
      _this.wsError('微信配置信息验证失败, 请使用浏览器打开网页');
    });

    window.wx.ready(() => {
      console.log('wx config ready');
      // 生产上注释
      // _this.setState({
      //   isCloseWs: false
      // });

      console.log('configSuccess: ', configSuccess);
      if (configSuccess) {
        _this.setState({
          isWXReady: true,
          isCloseWs: false
        });

        const employeeId = sessionStorage.getItem('employeeId');
        let linkUrl = window.origin + publicPath + '#login?refId' + employeeId;

        // 需在用户可能点击分享按钮前就先调用
        window.wx.updateAppMessageShareData({
          title: '嘉实浦惠AI培训师第一期',
          desc: '基金经理胡永青对嘉实浦惠的运作回顾及未来展望',
          link: linkUrl,
          imgUrl: window.location.origin + publicPath + 'redirect-img.jpeg',
          success: function () {
            console.log('微信分享设置成功');
          }
        });

        window.wx.startRecord();
        setTimeout(() => {
          window.wx.stopRecord();
        }, 3000);

        window.wx.onVoicePlayEnd({
          success: function (res) {
              console.log("播放结束",res);
              _this.setState({
                isVoiceEnd: new Date().getTime()
              })
          }
      });
      }
    });
  }

  // 超时逻辑
  speakTimeoutSet = () => {
    speakTimeoutTimer = setInterval(() => {
      if (speakTimerPause) {
        return;
      }

      speakTimerTimeout++;
      const timeoutNum = _.isNumber(window.$gVar.speak_timeout)
        ? window.$gVar.speak_timeout
        : 20;
      console.debug(`[timeout] 当前计时:超时时间`, `${timeoutNum}: ${speakTimerTimeout}`);

      if (speakTimerTimeout >= timeoutNum) { // 后台传的超时时间,默认20s
        this.resetSpeakTimeoutTimer();
        const sendRes = window.sendData({
          version: '1.0',
          sync: true,
          action_type: 'speak_timeout'
        });
        if (!sendRes) {
          return;
        }
        this.toggleRecorder();
      }
    }, 1000);
  }

  resetSpeakTimeoutTimer = (isPause = true) => {
    const branchType = _.get(this.state, 'branch.type', '');
    const isTopicSquareWidget = branchType === 'ait-question-index';
    if (isTopicSquareWidget) {
      isPause = true;
    }
    console.log('resetSpeakTimeoutTimer: ', isPause);
    speakTimerPause = isPause;
    speakTimerTimeout = 0;
  };

  toggleRecorder = (isShow = false) => {
    const states = ['isRecorderShow', 'isRecorderMount'];
    if (isShow) states.reverse();
    const updateState = {
      [states[0]]: isShow
    };
    this.setState(updateState, async () => {
      await wait(300);
      this.setState({
        [states[1]]: isShow
      });
    });
  };

  onRecorderStart = () => {
    speakStartTime = new Date().getTime();

    this.resetSpeakTimeoutTimer(true);
    this.setState({
      curSubtitle: null,
      subtitles: [],
      aitQActionShow: false,
      asrTime: 0,
      isRecording: true
    });
    clearTimeout(asrTimeout);
    asrTimeout = null;
  };

  onRecorderEnded = (isEnded = true) => {
    this.setState({
      durationTime: new Date().getTime() - speakStartTime
    });

    if (isEnded) {
        this.toggleRecorder(false);
        this.setState({
            aitQActionShow: true,
            recordLeftTime: recordMaxTime,
            isRecording: false
        });
    }

    if (this.state.curSubtitle !== null || !isEnded) {
      return;
    }

    asrTimeout = setTimeout(() => {
      Toast.fail('语音未能识别，请重新录制');
      this.setState({
        asrTime: 0,
        subtitles: [],
        curSubtitle: ''   // 区别于null, 传''时取消等待asr返回的loading状态
      });
      clearTimeout(asrTimeout);
      asrTimeout = null;
    }, 10 * 1000);
  };

  subtitleAction = subtitle => {
    if (asrTimeout) {
      clearTimeout(asrTimeout);
      asrTimeout = null;
    }
    
    const { subtitles } = this.state;
    let curSubtitle = subtitles.join('');

    if (subtitle.completed) {
      subtitles.push(subtitle.content);
    }
    curSubtitle += subtitle.content;
    
    this.setState({
      subtitles,
      curSubtitle
    });
  };

  videoAction = async (vData = {}) => {
    let currUrls = _.get(vData, 'videos.currentVideo.urls', '');
    let preFetches = _.get(vData, 'videos.preFetches', []);

    // 兼容后端只有一个就会把数组变成对象
    if (!Array.isArray(preFetches)) {
      preFetches = [preFetches].filter(el => el)
    }

    const updateState = {
      mainTitleText: vData.title || '',
      showSkip: vData.couldSkip === 'true',
      Step: vData.Step === 'true'
    };

    if (preFetches.length > 0) {
      updateState.preFetches = preFetches;
    }

    const isTraningWidget = _.get(vData.branch, 'type') === 'ait-scene-training';

    if (currUrls) {
      updateState.currentVideo = {
        id: _.get(vData, 'videos.currentVideo.id', ''),
        urls: currUrls
      };
    } else if (!isTraningWidget) {
      updateState.currentVideo = {
        id: '',
        urls: ''
      };
    }

    const hasVideo = !!currUrls;
    let ended;
    if (isTraningWidget) {
      ended = hasVideo ? false : this.state.videoQueEnded;
    } else {
      ended = !hasVideo;
    }

    const updateWidget = () => {
      this.setState(updateState, async() => {
        this.resetSpeakTimeoutTimer(!ended);
      });
    }

    this.setState({
      videoQueEnded: ended
    }, updateWidget);

    this.branchAction(vData.branch);
  };

  branchAction = branch => {
    this.setState({ branch });
    if (!branch) {
      return;
    }

    this.setState({
      subtitles: [],
      curSubtitle: null
    });

    if (branch.type === 'ait-topic-square') {
      this.resetSpeakTimeoutTimer(true);
    }

    if(branch.type === 'ait-question-echo'){
      this.setState({ isStep: true });
    }
  }

  popupAction = data => {
    this.setState({popupData: data});
  }

  onVideoStart = () => {
    clearTimeout(vdieoPlayTimer);
    this.updateVideoQueEnded(false);
    this.resetSpeakTimeoutTimer(true);

    vdieoPlayTimer = setTimeout(() => {
      window.sendData({
        "version": "1.0",
        "action_type": "log",
        "sync": true
      })
    }, 60 * 1000)
  }

  onVideoEnded = () => {
    this.updateVideoQueEnded(true);
    this.resetSpeakTimeoutTimer(false);
  };

  // 显示培训场景演练的提交页面
  updateActionShow = show => {
    this.setState({ aitQActionShow: show });
  };

  updateVideoQueEnded = end => {
    this.setState({
      videoQueEnded: end
    });
  }

  updateRecordLeftTime = time => {
    this.setState({
      recordLeftTime: this.state.recordLeftTime + time
    });
  }

  wsError = (msg, code) => {
    this.setState({
      isErrorModalShow: true,
      errorModalTitle: code ? `错误：${code}` : '',
      errorModalContext: msg
    });
  };

  render() {
    const {
      isRecorderShow, isWXReady, mainTitleText, currentVideo, preFetches,
      isRecorderMount, isSupRecorder, isCloseWs, showSkip, popupData,
      isErrorModalShow ,errorModalContext, errorModalTitle,
      branch, aitQActionShow, curSubtitle, videoQueEnded, recordLeftTime, isRecording,isStep,
      durationTime, isVoiceEnd, Step
    } = this.state;

    return (
        <div className="w-video">
          {!isCloseWs && (
            <StompCon
              {...this.props}
              isRecording={isRecording}
              subtitleAction={this.subtitleAction}
              videoAction={this.videoAction}
              popupAction={this.popupAction}
              onError={this.wsError}
              faqAction={this.faqAction}
            />
          )}

          <MainTitle text={mainTitleText}/>

          <Video
            widget={branch}
            currentVideo={currentVideo || {}}
            preFetches={preFetches}
            showSkip={showSkip}
            onStart={this.onVideoStart}
            onEnded={this.onVideoEnded}
            isStep={isStep}
            Step={Step}
          />

          {isSupRecorder && isRecorderMount && (
            <Recorder
              isShow={isRecorderShow}
              isWXReady={isWXReady}
              recordLeftTime={recordLeftTime}
              onStart={this.onRecorderStart}
              onEnded={this.onRecorderEnded}
              updateRecordLeftTime={this.updateRecordLeftTime}
            />
          )}

          <Widget
            widget={branch}
            isSupRecorder={isSupRecorder}
            isWXReady={isWXReady}
            aitQActionShow={aitQActionShow}
            curSubtitle={curSubtitle}
            recordLeftTime={recordLeftTime}
            videoQueEnded={videoQueEnded}
            onStart={this.onRecorderStart}
            onEnded={this.onRecorderEnded}
            toggleRecorder={this.toggleRecorder}
            updateActionShow={this.updateActionShow}
            showSkip={showSkip}
            isStep={isStep}
            Step={Step}
            durationTime={durationTime}
            isVoiceEnd={isVoiceEnd}
          />

          <Popup popupData={popupData} />

          <ErrorModal
            isShow={isErrorModalShow}
            title={errorModalTitle}
            content={errorModalContext}
            link=""
            confirmText="确定"
            onCancel={reload}
            closeAction={reload}
          />
        </div>
    );
  }
}
