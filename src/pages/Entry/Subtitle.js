import React, {useEffect} from 'react'
import dayjs from 'dayjs'

export default function (props) {
  const {subtitles} = props;
  useEffect(() => {
    const outWrapEle = document.getElementById('subtitleOutWrap');
    if(outWrapEle && outWrapEle.scrollTo){
      outWrapEle.scrollTo({
        top: outWrapEle.scrollHeight,
        behavior: "smooth"
      });
    }

  }, [subtitles]);

  return (
      <ul className="subtitle-wrap" id="subtitleOutWrap">
        {subtitles.map((el, idx) => (
            <li className="item" key={idx}>
              <div className="content">
                <div className="time">{dayjs(el.time).format('HH:mm')}</div>
                <div className="text">
                  {el.text || ''}
                </div>
              </div>
            </li>
        ))}
      </ul>
  )
}
