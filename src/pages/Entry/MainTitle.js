import React, { useState, useEffect } from 'react'
import { CSSTransition } from 'react-transition-group'

export default function (props) {
  const [isIn, setIsIn] = useState(false);
  const [title, setTitle] = useState('');

  useEffect(() => {
    setIsIn(false)
    setTimeout(() => {
      setIsIn(true)
      setTitle(props.text)
    }, 500);
  }, [props.text])

  return (
  
    <CSSTransition
      in={isIn}
      timeout={500}
      classNames='fade'
    >
      {
        title
        ? <div className="main-title" dangerouslySetInnerHTML={{__html:title}} />
        : <></>
      }
      
    </CSSTransition>
  );
}
