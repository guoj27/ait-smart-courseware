// eslint-disable-next-line no-unused-vars
import React, { useState, useEffect } from 'react';
import {Client} from "@stomp/stompjs";
import {Toast} from "antd-mobile";
import { getWebsocketUrl } from 'util';
import dayjs from "dayjs";
import CloseWarningModal from 'components/ErrorModal/CloseWarningModal';

let stompClient = null;
let closeTip = '';
let sendDataTimer = null;

let timeout = 2 * 60 - 10;     // 后端超时为2分钟，前端超时为1分50秒，会提前10秒告知用户即将超时
let warningTimeout = timeout - 11;

/**
 *
 * @param { subtitleAction, videoAction } props
 * @returns null
 */

export default function (props) {
  const [warningModalShow, setWarningModalShow] = useState(false);
  const { onError = () => {}, isRecording } = props;

  const sendData = bodyData => {
    if (sendDataTimer) {
      clearTimeout(sendDataTimer);
      sendDataTimer = null;
    }

    try {
      const wData = JSON.stringify(bodyData);
      stompClient.publish({
        destination: '/app/data',
        body: wData
      });
    } catch (e) {
      console.error("body is wrong", bodyData, e.name, e.message);
      onError("服务器断线，请重试");
    }

    if (isRecording) {
      return true;
    }

    if (warningTimeout) {
      sendDataTimer = setTimeout(() => {
        setWarningModalShow(true);
      }, warningTimeout * 1000);
    }

    return true;
  };

  const webrtcSubscribe = frame => {
    console.log("<<<webrtc", frame.body);
    let body = {};
    try {
      body = JSON.parse(frame.body);
      if (!(body && body.type)) return;
    } catch (e) {
      console.error(e)
    }
    const {type} = body;

    if (type === "CONNECT") {
      if (body.success) {
        window.sendData({
          "version": "1.0",
          "action_type": "log",
          "sync": true,
          "extra": {
            "EventType": "WebsocketID SetValue",
            "OldWebsocketID": window.$gVar.sessionId,
            "OldRequestID": window.$gVar.requestId,
            "WebsocketID": body.sessionId,
            "RequestID": body.requestId,
            "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
          }
        });

        window.$gVar.requestId = body.requestId;
        window.$gVar.sessionId = body.sessionId;

        const employeeId = sessionStorage.getItem("employeeId");
        const refId = sessionStorage.getItem("refId");

        window.sendData({
          text: '',
          action_type: 'start_meeting',
          extra: {
            form: {
              global: {
                AID: window.$gVar.AID,
                employeeId,
                version: "v1"
              }
            }
          }
        });

        window.sendData({
          version: "1.0",
          action_type: "asr",
          text: "",
          extra: {
            form: {
              global: {
                asr_mute: 1     //1 禁止agent直接发送asr结果，0启用 agent发送asr结果给aicp
              }
            }
          }
        });

        if (refId) {
            window.sendData({
                version: '1.0',
                action_type: 'log',
                sync: true,
                extra: {
                  EventType: 'SendRefId',
                  EmployeeId: employeeId,
                  RefId: refId,
                  WebsocketID: body.sessionId,
                  dialog_node_id: 'new',
                  RequestID: body.requestId,
                  ClientTimeStamp: dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
                }
            });
        }

        setTimeout(() => {
          // tag: log connect事件
          const platformMap = (GPUInfo = '') => {
            const platformArr = [];
            const platformArr2 = [];
            const platformArr3 = [];
            const map = {
              '11_pro_max': 'Apple A13 GPU',
              '11_pro': 'Apple A13 GPU',
              '11': 'Apple A13 GPU',

              'xs_max': 'Apple A12 GPU',
              'xs': 'Apple A12 GPU',
              'xr': 'Apple A12 GPU',

              'x': 'Apple A11 GPU',
              '8': 'Apple A11 GPU',
              '8_plus': 'Apple A11 GPU',

              '7': 'Apple A10 GPU',
              '7_plus': 'Apple A10 GPU',

              '6s_plus': 'Apple A9 GPU',
              '6s': 'Apple A9 GPU',
              'se': 'Apple A9 GPU',

              '6': 'Apple A8 GPU',
              '6_plus': 'Apple A8 GPU',

              '5s': 'Apple A7 GPU',

              '5': 'Apple A6 GPU',

              '4s': 'Apple A5 GPU',

              '4': 'Apple A4 GPU',

            };
            Object.keys(map).forEach(key=>{
              const value = map[key];
              if(GPUInfo.indexOf(value) !== -1){
                platformArr.push(key)
              }
            });

            if(window.screen.height === 667){
              // 6/6s/7/8
              platformArr2.push('6');
              platformArr2.push('6s');
              platformArr2.push('7');
              platformArr2.push('8');
            }else if(window.screen.height === 736){
              // 6p/6sp/7p/8p
              platformArr2.push('6_plus');
              platformArr2.push('6s_plus');
              platformArr2.push('7_plus');
              platformArr2.push('8_plus');
            }else if(window.screen.height === 812){
              // x/xs/11p
              platformArr2.push('x');
              platformArr2.push('xs');
              platformArr2.push('11_pro');
            }else if(window.screen.height === 896){
              // xr/xs max/11/11pm
              if(window.devicePixelRatio === 2){
                // xr/11
                platformArr2.push('xr');
                platformArr2.push('11');
              }else if(window.devicePixelRatio === 3){
                // xs max/11 pro max
                platformArr2.push('xs_max');
                platformArr2.push('11_pro_max');
              }
            }

            for (let i = 0; i < platformArr2.length; i++) {
              const element = platformArr2[i];
              const findOne = platformArr.find(el=>element === el);
              if(findOne) platformArr3.push(findOne);
            }
            return platformArr3.length ? platformArr3.map(el=>'iPhone ' + el.replace(/_/g,' ')).join('|') : 'unknown'
          };
          const Device = platformMap(window.$gVar.GPUInfo);
          console.log(`Device`,Device);
          window.sendData({
            "version": "1.0",
            "action_type": "log",
            "sync": true,
            "extra": {
              "EventType": "Connect",
              "WebsocketID": body.sessionId,
              "dialog_node_id": "new",
              "RequestID": body.requestId,
              "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'),
              "User-Agent": window.navigator.userAgent,
              "GPU-Info": window.$gVar.GPUInfo,
              "Screen-Width": window.screen.width,
              "Screen-Height": window.screen.height,
              "Device-Pixel-Ratio": window.devicePixelRatio,
              "Device": Device,
              "is_lite_mode": !!window.$gVar.isSupRecorder
            }
          });
        }, 200);
        const connectFail = localStorage.getItem('connectFail');
        if (connectFail) {
          // tag: log ConnectFail事件
          window.sendData({
            "version": "1.0",
            "action_type": "log",
            "sync": true,
            "extra": {
              "EventType": "ConnectFail",
              "WebsocketID": body.sessionId,
              "dialog_node_id": "new",
              "RequestID": body.requestId,
              "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'),
              "User-Agent": window.navigator.userAgent,
              "Body": connectFail
            }
          });
          localStorage.removeItem('connectFail')
        }

        // test code: ping pong
        window.sendPing = (data = {}) => {
          if(typeof data === 'string' || typeof data === 'number' || Array.isArray(data)){
            data = {
              data
            }
          }
          window.sendData({
              "version": "1.0",
              "action_type": "ping",
              "extra": {
                ...data,
                client_timestamp: new Date().getTime()
              }
            }
          )
        }
      } else {
        localStorage.setItem('connectFail', JSON.stringify(body));
        Toast.fail(<div>连接失败<br/> {body.message}</div>)
      }
    }
  };

  const dataSubscribe = frame => {
    console.log("<<<data", frame.body);
    try {
      if(!frame.body) return;
      const body = JSON.parse(frame.body);
      if (body) {
        const {voiceText, type, data, widget = {}, debug, action_type} = body;
        const { speak_timeout } = widget;

        if (voiceText){
          return
        }

        if(action_type === 'pong'){
          console.log(body.extra)
        }
        if (debug) {
          console.debug('--- time --- debug: ', dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'));
          console.error(`sessionId:${window.$gVar.sessionId}`, `dialog_node_id:${window.$gVar.dialog_node_id}`, debug);
          window.sendData({
            "version": "1.0",
            "action_type": "log",
            "sync": true,
            "extra": {
              "EventType": "debug",
              "WebsocketID": body.sessionId,
              "dialog_node_id": window.$gVar.dialog_node_id,
              "RequestID": body.requestId,
              "Debug": JSON.stringify(debug),
              "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
            }
          });
          const _code = parseInt(debug.code);
          if(![2002, 2003, 2004].includes(_code)){
            onError(`小浦开小差了，请重试`, debug.code);
            return
          } else {
            // window.sendData({
            //   "version": "1.0",
            //   "action_type": "text",
            //   "text": "asr_空意图"
            // });
          }
        }
        if (speak_timeout) {
          window.$gVar.speak_timeout = parseInt(speak_timeout);
          if (!isNaN(window.$gVar.speak_timeout)){
            window.$gVar.speak_timeout = window.$gVar.speak_timeout / 1000;
          }
        } else {
          window.$gVar.speak_timeout = null
        }

        if (type === 'SUBTITLE' && props.subtitleAction) {
          props.subtitleAction(JSON.parse(data))
        }
        
        if (widget.type === 'sales-promotion-video' && props.videoAction) {
          window.$gVar.dialog_node_id = widget.data.dialog_node_id || '';
          props.videoAction(widget.data)
        }

        if (widget.type === 'popup') {
          props.popupAction(widget.data);
        }
      }
    } catch (e) {
      console.error(e);
    }
  };

  const createStompClient = () => {
    const token = sessionStorage.getItem("token");
    stompClient = new Client({
      brokerURL: localStorage.getItem('socketUrl') || getWebsocketUrl(token),
      reconnectDelay: 0,
      // heartbeatIncoming: 4000, // 默认10s
      // heartbeatOutgoing: 4000,
      splitLargeFrames: true,
      maxWebSocketChunkSize: 32 * 1024,
      // connectHeaders:{token:sessionStorage.getItem("token")},
      onConnect: () => {
        stompClient.subscribe("/user/queue/webrtc", webrtcSubscribe);
        stompClient.subscribe("/user/queue/data", dataSubscribe);
        
        stompClient.publish({
          destination: "/app/webrtc",
          body: JSON.stringify({
            type: "CONNECT",
            auth: {
              token: '',
              signature: '',
              customerId: '',
            },
            tenant: 'dev',
            customizedItems: {
              userId: 'test',
              connectId: localStorage.getItem('connectSessionId')
            }
          })
        });
      },
      onWebSocketError: e => {
        console.log("on websocket error", e);
        onError("服务器断线，请重试");
      },
      onWebSocketClose: CloseEvent => {
        console.log("on websocket close", CloseEvent.code);
        if (!navigator.onLine) {
          // onError(`网络连接断开了(${CloseEvent.code})`);
          createStompClient();
        } else {
          console.log(closeTip,'closeTip');
            onError(closeTip || `服务连接已关闭 (${CloseEvent.code})`);     
        }
        window.$gVar.noSleep && window.$gVar.noSleep.disable();
        window.$gVar.noSleep = null;
      },
      onStompError: frame => {
        Toast.fail(frame.body || '数据异常');
      },
      debug: str => {
        console.log(str + ' |' + dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'));
      }
    });
    stompClient.activate();
  }

  const closeCon = msg => {
    clearTimeout(sendDataTimer);
    sendDataTimer = null;
    closeTip = msg;
    if (stompClient.connected) {
      window.sendData && window.sendData({
        "version": "1.0",
        "action_type": "log",
        "sync": true,
        "extra": {
          "EventType": "WebsocketID ClearValue",
          "WebsocketID": window.$gVar.sessionId,
          "RequestID": window.$gVar.requestId,
          "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
        }
      });
      stompClient.deactivate();
    }
    stompClient = null;
    window.$gVar.sessionId = null;
    window.$gVar.requestId = null;
    window.sendData = () => {};
  }

  useEffect(() => {
    createStompClient();
    window.sendData = sendData;
    window.closeConnect = closeCon;

    return () => {
      closeCon();
      closeTip = null;
      timeout = null;
      warningTimeout = null;
    };
    // eslint-disable-next-line
  }, []);

  return (
    <CloseWarningModal
      visible={warningModalShow}
      onClose={() => setWarningModalShow(false)}
    />
  );
}
