import React, { useState, useEffect, useCallback } from 'react'
import { Button } from 'antd-mobile'
import { CSSTransition } from "react-transition-group";

import useInterval from 'hooks/useInterval';
import recorder from 'hooks/recorder';

import './style.scss';

const dur = 1000;    // 1s
let startRecordTime = 0;

export default function (props) {
  const [isTouchEnd, setIsTouchEnd] = useState(false);
  const [isMaskShow, setIsMaskShow] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const [isMediaError, setIsMediaError] = useState(props.isWXReady ? false : true); // 是微信的话就不用延迟2s显示录音
  const [duration, setDuration] = useState(null);

  const onCreateMediaSuccess = () => {
    setIsMediaError(false);
  }

  const onCreateMediaFail = err => {
    setIsMediaError(true);
  }

  const handleRecordEndBusiness = () => {
    setIsMaskShow(false);
    setDuration(null);
    setIsTouchEnd(false);
    props.onEnded(new Date().getTime() - startRecordTime >= 1000);
  }

  const { startRecord, stopRecord } = recorder(props.isWXReady)(
    onCreateMediaSuccess,
    onCreateMediaFail,
    handleRecordEndBusiness       // 微信录音超过1分钟会自动停止，此方法处理停止录音后的业务逻辑
  );

  const intervalCallback = useCallback(() => {
    if (props.recordLeftTime <= 0) {
      setDuration(null);
      handleRecordEndBusiness();
      return;
    }
    props.updateRecordLeftTime(-dur/1000);
    /* eslint-disable-next-line */
  }, [props.recordLeftTime]);

  useInterval(intervalCallback, duration);

  const touchStart = e => {
    e.preventDefault();
    setIsMaskShow(true);
    setDuration(dur);  // 1s
    setIsTouchEnd(true);
    startRecord();
    props.onStart();
    startRecordTime = new Date().getTime();
  };

  // 防止 touchend 不触发
  const touchMove = e => {
    e.preventDefault();
  }

  const touchEnd = () => {
    stopRecord();
    handleRecordEndBusiness();
  };

  useEffect(() => {
    setIsShow(props.isShow);
    return () => {
      startRecordTime = null;
    }
  },[props.isShow]);

  const getRecorderShow = () => {
    if (isShow && !isMediaError) {
      return true;
    }
    return false;
  }

  const isRecorderShow = getRecorderShow();

  return (
      <>
        <div className={`recorder-mask ${isMaskShow ? 'show':''}`}>
          <div className="text">
            长按并回答<br />
            说完后松开
          </div>
        </div>

        <CSSTransition in={isRecorderShow} timeout={300} classNames='fadeIn'>
          <div className="recorder-wrap">
          <div className="tip">
              {isTouchEnd ? "请在"+ props.recordLeftTime +"内回答完毕":''}
            </div>
            <Button
              className={`btn ${isTouchEnd ? '' : 'flash-ani'}`}
              onTouchStart={touchStart}
              onTouchMove={touchMove}
              onTouchEnd={touchEnd}
            >
              {/*兼容安卓浏览器长按出现下载*/}
              <span className="bg"/>
            </Button>
            <div className="tip">
              {isTouchEnd ? '':'按住 说话'}
            </div>
            {
              isTouchEnd ? (
                  <div className="halo-wrap">
                    <div className="halo item-1" style={{borderColor:window.fundcolor,boxShadow: `0 0 10px 0 ${window.fundcolor}`}}></div>
                    <div className="halo item-2" style={{borderColor:window.fundcolor,boxShadow:`0 0 10px 0 ${window.fundcolor}`}}></div>
                    <div className="halo item-3" style={{borderColor:window.fundcolor,boxShadow:`0 0 10px 0 ${window.fundcolor}`}}></div>
                  </div>
              ) : null
            }
          </div>
        </CSSTransition>

        {isShow && isMediaError && (
          <div className="recorder-wrap" style={{opacity: 1, zIndex: 4, bottom: 80}}>
            <div className="recorder-call-tip">
              正在调起麦克风...
            </div>
          </div>
        )}
      </>
  );
}
