import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'antd-mobile';

export default props => {
  const [commonVisible, setCommonVisible] = useState(false);
  const [data, setData] = useState({});

  const { popupData } = props;

  useEffect(() => {
    if (popupData) {
      setData(popupData);
      setCommonVisible(true);
    }
  }, [popupData]);

  const closeModal = () => {
    setCommonVisible(false);
  }

  const handleAction = text => {
    closeModal();
    window.sendData({
      version: "1.0",
      action_type: "click",
      text: `press_${text}`
    });
  }

  const commonModal = () => {
    const { title, body, confirmText, cancelText } = data;
    return (
      <Modal
        transparent
        className="sup-modal"
        visible={commonVisible}
        maskClosable={false}
        onClose={closeModal}
      >
        <div className="title">{title}</div>
        <div className="content">{body}</div>
        <div className="footer pop-footer">
          {cancelText && (
            <Button className="button cancel-btn" onClick={() => handleAction(cancelText)}>
              {cancelText}
            </Button>
          )}
          <Button className="button confirm-btn" onClick={() => handleAction(confirmText)}>
            {confirmText}
          </Button>
        </div>
      </Modal>
    );
  };

  return commonVisible ? commonModal() : null;
}