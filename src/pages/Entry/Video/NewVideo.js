import React, { useState, useEffect, useRef, useCallback } from 'react';

import { wait, isIOS, isWeChat } from 'util';
// import ErrorModal from 'components/ErrorModal';
import CSSFade from 'components/CSSFade';

import videoBlankBg from 'static/simu/bg.png';
import videoFakeBg from 'static/simu/bg.png';
import videoPoster from 'static/simu/bg.png';

let autoPlayCheckTimer = null;
let visibilityEventBinded = false;
let showSkipTimer = null;

export default props => {
  const [curVideo, setCurVideo] = useState('');
  const [cacheVideo, setCacheVideo] = useState([]);
  const [isSwitchVideo, setIsSwitchVideo] = useState(true);
  const [showPlay, setShowPlay] = useState(false);
  const [isAutoPlay, setIsAutoPlay] = useState(false);

//   const checkedAutoplay = useRef(false);
  const videoRef = useRef(null);
  const video = videoRef.current;

  const { currentVideo, preFetches, widget = {} } = props;

  useEffect(() => {
    if (!isIOS && isWeChat) {
      playingVideoOnWechat();
    }

    setShowPlay(true);

    return () => {
      document.removeEventListener('visibilitychange');
      if (autoPlayCheckTimer) {
        clearTimeout(autoPlayCheckTimer);
        autoPlayCheckTimer = null;
      }
      if (showSkipTimer) {
        clearTimeout(showSkipTimer);
        showSkipTimer = null;
      }
    }
    /* eslint-disable-next-line */
  }, []);

  const playingVideoOnWechat = useCallback(() => {
    if (window.WeixinJSBridge) {
      window.WeixinJSBridge.invoke('getNetworkType', {}, function (e) {
        video && video.play();
      }, false);
    }
  }, [video]);

  useEffect(() => {
    if (visibilityEventBinded) {
      return;
    }

    document.addEventListener('visibilitychange', e => {
      const isHidden = e.target.hidden;
      console.log('# visiblityChange isHidden: ', isHidden);
      if (!isHidden) {
        if (video && video.paused) {
          // setShowErr(true);
          console.log('# video play excuted');
          if (isIOS && isWeChat) {
            playingVideoOnWechat();
          } else {
            video && video.play();
          }
        }
      }
    });
    visibilityEventBinded = true;
  }, [video, playingVideoOnWechat]);

//   const checkAutoPlay = async() => {
//     if (checkedAutoplay.current) {
//       return;
//     }

//     // 4秒不播放就显示播放按钮
//     autoPlayCheckTimer = setTimeout(() => {
//       checkedAutoplay.current = true;
//       if (video.currentTime === 0) {
//         setShowPlay(true);
//       }
//     }, 4000);
//   };

  // 延时切换背景图片，因为安卓微信上视频播放初始会黑屏或者闪烁
  const switchBg = async() => {
    while(video.currentTime === 0) {
      await wait(50);
    }
    await wait(500);
    setIsSwitchVideo(false);
  };

  useEffect(() => {
    if (!currentVideo) {
      return;
    }
    setIsSwitchVideo(true);  // 再次确认隐藏视频
    setCurVideo(currentVideo.urls);
    if (isAutoPlay) {
      playingIOSVideoOnWechat();    // ios 微信录音后视频不会自动播放
    }
  /* eslint-disable-next-line */
  }, [currentVideo]);

  useEffect(() => {
    if (!preFetches) {
      setCacheVideo([]);
      return;
    }
    const toCache = [];
    preFetches.forEach(({ id, urls }) => {
      if (urls) {
        toCache.push(urls);
      }
    });
    setTimeout(() => {
      setCacheVideo(toCache);
    }, 3000);
  }, [preFetches]);

  const handleShowSkip = (delay, isSkip) => {
    if (!isAutoPlay && !isSkip) {
        return;
    }
  }

  useEffect(() => {
    if (autoPlayCheckTimer) {
      clearTimeout(autoPlayCheckTimer);
      autoPlayCheckTimer = null;
    }
    handleShowSkip(2000);
  /* eslint-disable-next-line */
  }, [widget.type, currentVideo]);

  const playingIOSVideoOnWechat = () => {
    if (isIOS && isWeChat) {
      playingVideoOnWechat();
    }
  }

  const onVideoPlay = async() => {
    props.onStart();
    switchBg();
  }

  const onVideoEnded = () => {
    // setShowSkip(false);
    setIsSwitchVideo(true);
    video && video.pause();
    props.onEnded();
  }

  const onVideoPlayError = () => {
    console.error('video play error');
    onVideoEnded();
  }

  const onPlayBtnClick = () => {
    video && video.play();
    setShowPlay(false);
    setIsAutoPlay(true);
    handleShowSkip(2000, true);
  }

  const cdnPath = window.$gVar.cdnPath;
  return (
    <>
      <div className="video-bg-wrap">
        <video
          id="processVideo"
          ref={videoRef}
          className={`v-ele ${isSwitchVideo ? 'v-hidden' : ''}`}
          src={curVideo ? `${cdnPath}${curVideo}` : ''}
          onPlay={onVideoPlay}
          onEnded={onVideoEnded}
          onError={onVideoPlayError}
          poster={videoPoster}
          controls={true}
          preload="auto"
          autoPlay={isAutoPlay}
          playsInline
          webkit-playsinline="true"
          x5-video-player-fullscreen="false"
          x5-video-player-type="h5"
        />

        {/* 视频切换过渡背景 */}
        <img
            src={curVideo ? videoFakeBg : videoBlankBg}
            alt=""
            className="video-fake-bg bot"
        />

        {/* 预加载视频 */}
        {cacheVideo.map((item, i) => (
          <video
            muted
            hidden
            key={i}
            preload="auto"
            src={cdnPath + item}
          />
        ))}
      </div>

      <CSSFade visible={showPlay}>
        <div className="play-btn" onClick={onPlayBtnClick}>
          播 放
        </div>
      </CSSFade>

      {/* <ErrorModal
        isShow={showErr}
        content="抱歉哟！<br />刚才小浦和您的互动还没结束就被打断了呢！"
        confirmText="继续"
        closeAction={() => setShowErr(false)}
      /> */}
    </>
  );
}