import { useEffect } from 'react';

export default props => {
  const { onQueueStart, queueVideos } = props;
  const queueLength = queueVideos.length;

  useEffect(() => {
    if (queueLength > 0) {
      onQueueStart();
    }
  /* eslint-disable-next-line */
  }, [queueLength]);
  return null;
}