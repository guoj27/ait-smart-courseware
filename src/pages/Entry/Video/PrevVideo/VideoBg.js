import React from 'react';
import { Toast } from "antd-mobile";
import watchProps from 'watch-props'
import dayjs from "dayjs";
import _ from 'lodash';

import videoFakeBg from 'static/simu/bg.png';
import ErrorModal from 'components/ErrorModal';
import VideoWatch from './VideoWatch';

let timerID;
let BG_W = window.screen.width; // 默认视频宽高为屏幕宽高
let BG_H = window.screen.height;
let canvasCtx = null;
let cacheVideosQueue = [];
// let downloadCacheFlag = false;
let videoEndedFlag = false;

/**
 *
 * @param currentVideo Object {id:String,urls:[String]}
 * @param preFetches Array [String]
 * @param idleVideos Array [String]
 * @returns {*}
 */

class VideoBg extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentVideoUrl: '',
      cacheVideos: [],
      queueVideos: [],
      isSwitchVideo: false,
      isErrorShow: false
    }
  }

  componentDidMount() {
    const canvasEle = document.getElementById('videoCanvas');
    if (canvasEle) {
      canvasCtx = canvasEle.getContext('2d');
      BG_W = canvasEle.clientWidth;
      BG_H = canvasEle.clientHeight;
    }
    this.updateCache();

    setTimeout(() => {
      this.playingVideo();
    }, 500);

    const videoWrapEl = document.getElementsByClassName('video-bg-wrap')[0];

    const playhandler = () => {
      this.playingVideo();
      videoWrapEl.removeEventListener('touchstart', playhandler);
    }

    videoWrapEl.addEventListener('touchstart', playhandler);

    const pageVisibility = (function() {
      let prefixSupport, keyWithPrefix = function (prefix, key) {
        if (prefix !== "") {
          // 首字母大写
          return prefix + key.slice(0, 1).toUpperCase() + key.slice(1);
        }
        return key;
      };
      const isPageVisibilitySupport = (function () {
        let support = false;
        if (typeof window.screenX === "number") {
          ["webkit", "moz", "ms", "o", ""].forEach(function (prefix) {
            if (support === false && document[keyWithPrefix(prefix, "hidden")] !== undefined) {
              prefixSupport = prefix;
              support = true;
            }
          });
        }
        return support;
      })();

      const isHidden = function () {
        if (isPageVisibilitySupport) {
          return document[keyWithPrefix(prefixSupport, "hidden")];
        }
        return undefined;
      };

      const visibilityState = function () {
        if (isPageVisibilitySupport) {
          return document[keyWithPrefix(prefixSupport, "visibilityState")];
        }
        return undefined;
      };

      return {
        hidden: isHidden(),   // 当前tab窗口离开时为true，进入时为false
        visibilityState: visibilityState(),   // 当前tab窗口离开时为hidden，进入时为visible
        visibilityChange: function(fn, useCapture = false) {   // 当前tab窗口离开或者进入时会执行该事件
          if (isPageVisibilitySupport && typeof fn === "function") {
            return document.addEventListener(prefixSupport + "visibilitychange", function(evt) {
              this.hidden = isHidden();
              this.visibilityState = visibilityState();
              fn.call(this, evt);
            }.bind(this), useCapture);
          }
          return undefined;
        }
      };
    })();

    pageVisibility.visibilityChange(()=>{
      if (!pageVisibility.hidden) {   // 页面进入时才执行
        const { isErrorShow } = this.state;
        const videoEle = document.getElementById('playingVideo');
        if (videoEle && videoEle.paused) {
          if (!isErrorShow) {
            this.setState({
              isErrorShow: true
            });
          }
        } else {
          this.setState({
            isErrorShow: false
          });
        }
      }
    });
  }

  componentWillUnmount() {
    timerID = null;
    BG_W = null;
    BG_H = null;
    canvasCtx = null;
    cacheVideosQueue = null;
    videoEndedFlag = null;
  }

  updateCache = () => {
    const { currentVideoUrl, queueVideos } = this.state;
    const { currentVideo = {}, preFetches = [], idleVideos = [], onEnded = () => {} } = this.props;
    const { urls = [] } = currentVideo;
    // 未到播放时间的压入栈里
    if (currentVideoUrl === '') {
      const cloneUrls = JSON.parse(JSON.stringify(urls));
      const firstVideo = cloneUrls.shift();
      this.setState({
        currentVideoUrl: firstVideo
      });
      for (let i = 0; i < cloneUrls.length; i++) {
        const url = cloneUrls[i];
        this.setState({
          queueVideos: [
            ...queueVideos,
            url
          ]
        });
      }
    } else {
      for (let i = 0; i < urls.length; i++) {
        const url = urls[i];
        this.setState({
          queueVideos: [
            ...queueVideos,
            url
          ]
        });
      }
    }

    onEnded(false);
    const uniqCacheVideos = (uUrl) => {
      const findOne = cacheVideosQueue.includes(uUrl); // 去重
      if (!findOne) {
        cacheVideosQueue.push(uUrl);
      }
      return findOne
    };
    // 缓存视频策略
    // 优先缓存idle视频
    for (let i = 0; i < idleVideos.length; i++) {
      const idleVideo = idleVideos[i];
      if (uniqCacheVideos(idleVideo)) {
        this.cacheVideoQueueShift();
      }
    }
    for (let i = 0; i < preFetches.length; i++) {
      const cacheUrls = _.get(preFetches, `[${i}].urls`, []);
      for (let j = 0; j < cacheUrls.length; j++) {
        const cacheUrl = cacheUrls[j];
        uniqCacheVideos(cacheUrl);
      }
    }
  };

  watch = {
    currentVideo() {
      this.updateCache()
    }
  };

  playingVideo = () => {
    const playingVideo = document.getElementById('playingVideo');
    playingVideo && playingVideo.play().then(() => {
      console.debug('--- time --- video play start: ', dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'));
      playingVideo.volume = 1;
    }).catch((err) => {
      console.debug('视频调用play失败：', err.name, err.message);
    });
  };

  videoEnded = () => {
    console.debug('--- time --- video play end: ', dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'));
    this.setState({
      isSwitchVideo: true
    });
    const {queueVideos} = this.state;
    const { idleVideos = [], onEnded = () => {} } = this.props;
    
    console.debug(`queueVideos:`, queueVideos);
    if (queueVideos.length > 0) {
      const cloneQueueVideos = JSON.parse(JSON.stringify(queueVideos));
      const firstUrl = cloneQueueVideos.shift();

      if (cloneQueueVideos.length === 0) {
        videoEndedFlag = true
      }
      const updateState = {
        queueVideos: cloneQueueVideos
      };
      updateState.currentVideoUrl = firstUrl || idleVideos[0] || '';
      
      this.setState(updateState);
      onEnded(false)
    } else {
      if (videoEndedFlag) {
        // tag: log Video_Play_Ended
        window.sendData({
          "version": "1.0",
          "action_type": "log",
          "sync": true,
          "extra": {
            "EventType": "Video_Play_Ended",
            "WebsocketID": window.$gVar.sessionId,
            "dialog_node_id": window.$gVar.dialog_node_id,
            "RequestID": window.$gVar.requestId,
            "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')
          }
        });
        videoEndedFlag = false;
      }
      const getOne = idleVideos[_.random(0, idleVideos.length - 1)];
      this.setState({
        currentVideoUrl: getOne || idleVideos[0] || ''
      });
      onEnded(true)
    }
    this.playingVideo();
  };

  drawImage = (video) => {
    if (canvasCtx) {
      const ratio = 540 / BG_W;
      const canvasHeight = Math.round( 960 / ratio);
      let offsetY = 0;
      if (canvasHeight < BG_H) { // 比屏幕短就往下偏移
        offsetY = BG_H - canvasHeight
      }
      try {
        canvasCtx.drawImage(video, 0, offsetY, BG_W, canvasHeight);
      } catch (e) {
        Toast.fail('该手机不支持canvas模式');
        console.error(e)
      }
    }
  };

  videoPlay = (e) => {
    this.setState({
      isSwitchVideo: false
    });
    const that = e.target;
    clearInterval(timerID);
    timerID = setInterval(() => {
      this.drawImage(that);
    }, 20);
  };

  cacheVideoQueueShift = () => {
    const {cacheVideos} = this.state;
    const firstUrl = cacheVideosQueue.shift();
    if (firstUrl) {
      this.setState({
        cacheVideos: Array.from(new Set([
          ...cacheVideos,
          firstUrl
        ]))
      });
    }
  };

  videoCanPlay = (cacheVideoUrl, e) => {
    // if (!downloadCacheFlag) return;
    this.cacheVideoQueueShift()
  };

  currentVideoCanPlay = () => {
    // 首个视频必须先能播放才需要预载入
    // downloadCacheFlag = true;
    this.cacheVideoQueueShift()
  };

  videoPlayError = (err) => {
    const {queueVideos} = this.state;
    // 播放错误就把视频弹出，再播放下一段
    const cloneQueueVideos = JSON.parse(JSON.stringify(queueVideos));
    if (cloneQueueVideos.length > 0) {
      cloneQueueVideos.shift();
      this.setState({
        queueVideos: cloneQueueVideos
      });
    }
    this.videoEnded();
    console.error(`video error: `, err.name, err.message)
  };

  onPlayBtnClick = () => {
    this.playingVideo();
  }

  errorModalClose = () => {
    this.setState({
      isErrorShow: false
    }, () => {
      this.playingVideo()
    });
  };

  render() {
    const {
      currentVideoUrl,
      // cacheVideos,
      isSwitchVideo,
      queueVideos,
      isErrorShow,
    } = this.state;

    const {
      isSupCanvasDrawVideo,
      idleVideos,
      onQueueStart,
    } = this.props;

    const cdnPath = window.$gVar.cdnPath || '';
    return (
        <>
          <div className="video-bg-wrap">
            <ErrorModal
              isShow={isErrorShow}
              content="抱歉哟！<br />刚才小浦和您的互动还没结束就被打断了呢！"
              confirmText="继续"
              closeAction={this.errorModalClose}
            />

            {isSupCanvasDrawVideo && (
              <canvas id="videoCanvas" className="v-canvas" width={BG_W} height={BG_H}/>
            )}

            {!isSupCanvasDrawVideo && (
              <img src={videoFakeBg} alt="" className={`video-fake-bg top ${isSwitchVideo ? 'show' : ''}`}/>
            )}

            {/* 当前播放的视频 */}
            <video
              id="playingVideo"
              className="v-ele"
              src={cdnPath + currentVideoUrl}
              preload="auto"
              autoPlay
              playsInline
              webkit-playsinline="true"
              x5-playsinline="true"
              x5-video-player-fullscreen="false"
              x5-video-player-type="h5-page"
              onEnded={this.videoEnded}
              onPlay={this.videoPlay}
              onError={this.videoPlayError}
              onCanPlayThrough={this.currentVideoCanPlay}
              hidden={isSupCanvasDrawVideo}
            >
              {queueVideos.map((value, index) => (
                <source src={cdnPath + value} key={`queueVideos_${index}`} type="video/mp4"/>
              ))}
              {idleVideos.map((value, index) => (
                <source src={cdnPath + value} key={`idle_${index}`} type="video/mp4"/>
              ))}
            </video>

            {!isSupCanvasDrawVideo && (
              <img src={videoFakeBg} alt="" className="video-fake-bg bot"/>
            )}

            <VideoWatch
              queueVideos={queueVideos}
              onQueueStart={onQueueStart}
            />
          </div>
          
          {/* {playBtnShow && !isErrorShow && (
            <Button
              className="play-btn"
              onClick={this.onPlayBtnClick}
            >
              播放
            </Button>
          )} */}
        </>
    );
  }
}

export default watchProps(VideoBg);
