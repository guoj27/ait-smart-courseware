import React from 'react';
import './style.scss';

export default function (props) {
  return (
      <div className="error-wrap">
        <div className="content">
          <div className="title">
            出错
          </div>
          <div className="msg">
            抱歉 ：(<br />
            暂时无法为您服务，<br />
            {props.match.params.msg || ''}。
          </div>
        </div>
      </div>
  );
}
