import React, { useEffect, useState } from 'react';
import './style.scss';

export default props => {
  const [csName, setCsName] = useState('');
  const { visible, children } = props;

  useEffect(() => {
    let timer = null;
    if (visible) {
      timer = setTimeout(() => {
        setCsName('ui-fade-in');
      }, 10);
    } else {
      setCsName('');
    }

    return () => {
      clearTimeout(timer);
    }
  }, [visible]);

  return visible
    ? <div className={`ui-fade ${csName}`}>
        {children}
      </div>
    : null
}
