import React, {useState} from 'react'
import './style.scss'
import {Button} from "antd-mobile";

export default function (props) {
  const [isShow, setIsShow] = useState(false)
  const isSupMediaDevices = !!window.navigator.mediaDevices;
  const [enumerateDevices, setEnumerateDevices] = useState('null')
  if (isSupMediaDevices) {
    window.navigator.mediaDevices.enumerateDevices().then((r) => {
      const devices = r.map(el => el.toJSON()).map(el => ({
        kind: el.kind,
        label: el.label
      }));
      setEnumerateDevices(JSON.stringify(devices, null, 4))
    })
  }

  const switchShow = function () {
    setIsShow(!isShow)
  };
  const versionEle = document.getElementById('version');
  return (
      <div className="debug-out-wrap">
        <div className={`debug-wrap ${isShow ? 'show' : ''}`}>
          <div className="item">
            <span>UA:</span>
            <span>{window.navigator.userAgent}</span>
          </div>
          <div className="item">
            <span>navigator.mediaDevices:</span>
            <span>{isSupMediaDevices ? 'true' : 'false'}</span>
          </div>
          <div className="item">
            <span>navigator.enumerateDevices:</span>
            <span>{enumerateDevices}</span>
          </div>
          <div className="item">
            <span>navigator.mediaDevices.getUserMedia:</span>
            <span>{window.navigator.mediaDevices.getUserMedia ? 'true' : 'false'}</span>
          </div>
          <div className="item">
            <span>navigator.getUserMedia:</span>
            <span>{window.navigator.getUserMedia ? 'true' : 'false'}</span>
          </div>
          <div className="item">
            <span>navigator.webkitGetUserMedia:</span>
            <span>{window.navigator.webkitGetUserMedia ? 'true' : 'false'}</span>
          </div>
          <div className="item">
            <span>navigator.mozGetUserMedia:</span>
            <span>{window.navigator.mozGetUserMedia ? 'true' : 'false'}</span>
          </div>
          <div className="item">
            <span>last audio is no wave:</span>
            <span>{props.isNoWave ? 'true' : 'false'}</span>
          </div>
          <div className="item">
            <span>last audio size:</span>
            <span>{props.wavSize / 1000 || 0}KB</span>
          </div>
          <div className="item">
            <span>version time:</span>
            <span>{versionEle ? versionEle.value : ''}</span>
          </div>
        </div>
        <Button className="btn" size="small" onClick={switchShow}>Debug</Button>
      </div>
  )
}
