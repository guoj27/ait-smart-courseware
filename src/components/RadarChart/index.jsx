import React, { useEffect, useRef, useMemo } from 'react';
import echarts from "echarts/lib/echarts";   // 按需加载
import "echarts/lib/chart/radar";    // 引入雷达图

import './index.scss';

let myChart = null;

export default props => {
  const { chartData, indicator } = props;
  const chartRef = useRef(null);

  // 解决雷达图中其他维度数据都是0的话，会不展示数据区域的问题
  const transedChartData = useMemo(() => {
    if (!chartData) {
      return null;
    }
    return chartData.map(item => item === '0' ? '0.1' : item);
  }, [chartData]);

  useEffect(() => {
    myChart = echarts.init(chartRef.current);
    return () => {
      myChart = null;
    }
  }, []);

  const option = useMemo(() => ({
    radar: {
      indicator: indicator,
      center: ['50%', '50%'],
      radius: '45%',     // 外直径比例
      splitNumber: 3,
      splitArea: {
        areaStyle: {
            color: ['transparent', 'transparent', 'transparent']
        }
      },
      splitLine: {
        show : true,
        lineStyle: {
            width: 1,
            color: '#ddd'
        }
      },
      axisLine: {
        lineStyle: {
            color: '#ddd',
        }
      },
      name: {
        formatter: function(value) {
          const [title, tip1, tip2] = value.split('，');
          return [`{a|${title}}\n{b|${tip1}}\n{b|${tip2}}`];
        },
        rich: {
          a: {
            color: '#000000',
            align: 'middle',
            fontWeight: 900,
            fontSize: 15,
          },
          b: {
            color: '#A8A8A8',
            fontSize: 12,
            align: 'middle',
          },
        },
      },
    },
    series: [{
      symbol: 'none',
      type: 'radar',
      areaStyle: {
        normal: {
          color: window.fundcolor, // 选择区域颜色
          opacity: 0.5
        }
      },
      lineStyle: { color: 'transparent' },
      data: [                                                              
        {
          value: transedChartData,
        }
      ]
    }
  ]}), [indicator, transedChartData]);

  useEffect(() => {
    if (myChart && transedChartData) {
      myChart.setOption(option);
    }
  }, [transedChartData, option]);

  return <div ref={chartRef} className="radar-chart" ></div>
}
