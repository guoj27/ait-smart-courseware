import React, { useState, useEffect } from 'react';

import './index.scss';

export default props => {
  const [checked, setChecked] = useState(false);

  const { defaultChecked } = props;

  useEffect(() => {
    if (defaultChecked) {
      setChecked(true);
    }
  }, [defaultChecked]);

  const onChange = () => {
    props.onChange(!checked);
    setChecked(!checked);
  }
 
  return (
    <span className="ait-checkbox" onClick={onChange}>
      <span className={`checkbox ${checked ? 'checkbox-checked' : ''}`} />
      <label>{props.children}</label>
    </span>
  );
}