import React, { useState, useCallback, useEffect } from 'react';
import { Modal, Button } from 'antd-mobile';
import dayjs from "dayjs";

import useInterval from 'hooks/useInterval';

export default props => {
  const [duration, setDuration] = useState(null); 
  const [leftTime, setLeftTime] = useState(10);
  const { visible, onClose } = props;

  const clear = useCallback(() => {
    setDuration(null);
    setLeftTime(10);
  }, []);

  useEffect(() => clear, [clear]);

  useEffect(() => {
    if (visible) {
      setDuration(1000);
    } else {
      clear();
    }
  }, [visible, clear]);

  const intervalCallback = useCallback(() => {
    setLeftTime(prev => {
      if (prev === 1) {
        setDuration(null);
        window.sendData({
          "version": "1.0",
          "action_type": "log",
          "sync": true,
          "extra": {
            "EventType": "FE_Close_Connect",
            "WebsocketID": window.$gVar.sessionId,
            "dialog_node_id": window.$gVar.dialog_node_id,
            "RequestID": window.$gVar.requestId,
            "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'),
          }
        });
        window.closeConnect('很抱歉，由于您长时间没有操作，AI培训师已经退出');
      }
      return prev <= 0 ? prev : prev - 1;
    });
  }, []);

  useInterval(intervalCallback, duration);

  const continueTraining = () => {
    window.sendData({
      "version": "1.0",
      "action_type": "log",
      "sync": true,
      "extra": {
        "EventType": "Cancel_Close_Connect",
        "WebsocketID": window.$gVar.sessionId,
        "dialog_node_id": window.$gVar.dialog_node_id,
        "RequestID": window.$gVar.requestId,
        "ClientTimeStamp": dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'),
      }
    });
    onClose();
  };

  return (
    <Modal
      transparent
      className="sup-modal"
      visible={visible}
      maskClosable={false}
    >
      <div className="title" />
      <div className="content">
        由于您长时间没有操作，<br/>
        AI培训师将在 <span>{leftTime}</span> 秒后自动退出
      </div>
      <div className="footer">
        <Button className="btn cus-btn" onClick={continueTraining}>
          继续培训
        </Button>
      </div>
    </Modal>
  );
}