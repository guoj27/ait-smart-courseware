import React from 'react';
import { Button, Modal } from "antd-mobile";

import './style.scss';

export default function (props) {
  const {
    isShow = false,
    title = '',
    content = '',
    closeAction,
    onCancel = () => {},
    confirmText = '确认',
    cancelText = '',
  } = props;

  return (
      <Modal
        className="sup-modal"
        visible={isShow}
        transparent
      >
        <div>
          <div className="title">
            {title}
          </div>
          <div className="content" dangerouslySetInnerHTML={{__html:content}} />
          <div className="footer">
            {closeAction &&
              <Button className="btn cus-btn" onClick={closeAction}>
                {confirmText}
              </Button>
            }
            {
              cancelText ? (
                <Button className="btn cus-btn bg-while" onClick={onCancel}>
                  {cancelText}
                </Button>
              ):null
            }
          </div>
        </div>
      </Modal>
  )
}
