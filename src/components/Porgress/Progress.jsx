import React, {useEffect, useState} from 'react';
import { Icon } from 'antd-mobile';
import './index.scss';

export default props =>  {
    const [currentProgress, setCurrentProgress] = useState(null);
    const [totalProgress, setTotalProgress] = useState(6);

    useEffect(() => {
        let current = currentProgress, total = totalProgress;
        if(props.widget.data){
            current = props.widget.data.currentProgress;
            total = props.widget.data.totalProgress;
            setCurrentProgress(current);
            setTotalProgress(total);
        }
        for( let i = 0;i<total;i++){
            if(i < current ){
                document.getElementById("li"+i).setAttribute("class","active");
            }
            if(i === current - 1){
                document.getElementById("li"+i).style.zIndex = 999;
            }
            // if(current === total){
            //     console.log(document.getElementById("finished"))
            //     document.getElementById("finished").setAttribute("class","finishedActive"); 
                
            // }
        }
    },[props]);
  
    return (
        <div className="progress-bg">
            <ul className="steps">
                <li id="li0"></li>
                <li id="li1"></li>
                <li id="li2"></li>
                <li id="li3"></li>
                <li id="li4"></li>
                <li id="li5"></li>
                {/* <li id="finished">
                    <span className="am-radio ">
                        <span className="am-radio-inner"></span>
                    </span>
                </li> */}
            </ul>
        </div>
    );
}