import React, { useState, useEffect } from 'react';

import CssFade from 'components/CSSFade';
import './index.scss';

export default ({ widget, visible }) => {
  const [current, setCurrent] = useState(null);
  const [points, setPoints] = useState(null);
  const [lines, setLines] = useState(null);

  const { totalProgress, currentProgress } = widget.data || {};
  const pTotal = parseInt(totalProgress, 10);
  const pCurrent = parseInt(currentProgress, 10);

  useEffect(() => {
    if (!pTotal) {
      return;
    }
    setPoints(() => {
      return new Array(pTotal).fill("point")
    });
    setLines(() => {
      return new Array(pTotal - 1).fill("line");
    })
  }, [pTotal]);

  useEffect(() => {
    if (!pCurrent) {
      return;
    }
    setCurrent(pCurrent);
  }, [pCurrent]);

  return (
    <CssFade visible={visible}>
      <div className="progress-wrap">
        <div className="progress">
          <ul className="step-line-list">
            {lines && lines.map((item, i) => (
              <li
                key={i}
                className={i < current - 1 ? 'active' : 'in-active'}
                style={{ width: `${100 / lines.length}%`}}
              />
            ))}
          </ul>

          <ul className="step-point-list">
            {points && points.map((item, i) => (
              <li
                key={i} 
                className={i < current ? 'active': ''}
              />
            ))}
          </ul>
        </div>
      </div>
    </CssFade>
    
  );
}
