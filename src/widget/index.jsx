import React, { useState, useEffect, useCallback } from 'react';

import CSSFade from 'components/CSSFade';
import CheatReminder from "./Rehearse/CheatReminder";
import TopicSquare from "./TopicSquare";
import Documents from "./Documents";
import Question from "./Question";
import Score from './score';

import './s-index.scss';

export default props => {
    const [cheatReminderVisible, setCheatReminderVisible] = useState(false);
    const [difficultScoreVisible, setDifficultScoreVisible] = useState(false);
    const [topicSquareVisible, setTopicSquareVisible] = useState(false);
    const [documentsVisible, setDocumentsVisible] = useState(false);
    const [questionVisible, setQuestionVisible] = useState(false);
    const [scoreVisible, setScoreVisilble] = useState(false);
    const {
      widget = {}, videoQueEnded,
      isSupRecorder, toggleRecorder, updateActionShow
    } = props;

    const { type } = widget;
    useEffect(() => {
      console.log('isSupRecorder | videoQueEnded: ', isSupRecorder, videoQueEnded);
      if (!isSupRecorder) {
        return;
      }
    /* eslint-disable-next-line */
    }, [widget]);

    const hiddenAll = useCallback(() => {
      setCheatReminderVisible(false);
      setDifficultScoreVisible(false);
      setTopicSquareVisible(false);
      setDocumentsVisible(false);
      setQuestionVisible(false);
      setScoreVisilble(false)
    }, []);

    useEffect(() => {
      hiddenAll();
      switch (type) {
        case "ait-cheat-reminder":
          setCheatReminderVisible(true);
          break;
        case "ait-total-score":
          setScoreVisilble(true);
          break;
        case 'topic':
          setTopicSquareVisible(true);
          break;
        case 'ait-database':
          setDocumentsVisible(true);
          break;
        case 'ait-question-index':
          setQuestionVisible(true);
          break;
        default:
      }
    }, [type, hiddenAll]);

    useEffect(() => {
      if (!isSupRecorder) {
        return;
      }

      if (
        type === "ait-scene-rehearse" ||
        type === "ait-scene-rehearse-reminder1"
      ) {
        updateActionShow(false);
      }

    /* eslint-disable-next-line */
    }, [props.widget]);

    const widthoutWidgetCss = [
      scoreVisible,
      difficultScoreVisible,
      topicSquareVisible,
      documentsVisible,
      questionVisible,
    ]
    let containerCs = widthoutWidgetCss.some(item => item) ? '' : 'widget-container';
    
    return (
      <div className="widget-wrap">
        <div className={containerCs}>
          <CSSFade visible={cheatReminderVisible}>
            <CheatReminder {...props} widget={widget} />
          </CSSFade>

          <CSSFade visible={scoreVisible}>
            <Score {...props} widget={widget} />
          </CSSFade>

          <CSSFade visible={topicSquareVisible}>
            <TopicSquare {...props} widget={widget} />
          </CSSFade>

          <CSSFade visible={documentsVisible}>
            <Documents {...props} widget={widget} />
          </CSSFade>

          <CSSFade visible={questionVisible}>
            <Question {...props} widget={widget}></Question>
          </CSSFade>

        </div>
      </div>
    );
}
