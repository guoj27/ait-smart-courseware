import React, {useRef, useState} from "react";
import { PhotoProvider, PhotoConsumer, PhotoSlider } from 'react-photo-view';
import 'react-photo-view/dist/index.css';

export default props => {

  const [flag, setFlag] = useState(false)
  

  const goBack = () => {
    setFlag(false)
    window.sendData({
      "version": "1.0",
      "action_type": "click",
      "text": "press_资料库返回"
    });
  } 

  return (
    <div className="doc-bg" onClick={() => setFlag(true)}>
      <PhotoProvider 
        maskClosable 
        photoClosable 
        visible={true}
        onClose={goBack}
      >
        <PhotoConsumer  photoClosable src={require('static/doc.png')} >
            <img src={require('static/doc.png')} alt="" />
        </PhotoConsumer>
      </PhotoProvider>
      
    </div>
  );
};
