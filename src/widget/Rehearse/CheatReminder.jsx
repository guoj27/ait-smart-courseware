import React from "react";

import { Button } from "antd-mobile";

export default props => {
  const { widget,  } = props;
  const { data = {} } = widget;

  const onContinue = () => {
    window.sendData({
      version: "1.0",
      action_type: "click",
      text: "知道了"
    });
  }

  return (
    <>
        <div className="blank-container common-blue-container ait-cheat-reminder"  style={{bottom: '20vh'}}>
          <header>{data.title}</header>
          <div className="ait-content cheat-reminder-content">
            {data.reminder}
          </div>
          <div className="cheat-reminder-action">
            <Button
              className="btn cus-btn"
              type="primary"
              onClick={onContinue}
            >
              {data.btn}
            </Button>
          </div>
        </div>
    </>
  );
}
