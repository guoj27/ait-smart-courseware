import React, { useEffect, useState, useRef } from 'react';
import { Icon, ListView, Toast } from "antd-mobile";

import Speak from 'widget/TopicSquare/Speak';
import answerIcon from 'static/icon/answerIcon.png'
import likeIcon from 'static/icon/like-icon.svg';
import likedIcon from 'static/icon/liked-icon.svg';

import api from 'axios/api';

const pageSize = 20;

export default props => {
  const listDataSource  = new ListView.DataSource({
    rowHasChanged: (row1, row2) => true,
  });

  const [pageNum,setPageNum] = useState(1);
  const [loading, setLoading] = useState(true);
  const [totalAnswerCounts,setTotalAnswerCounts] = useState();
  const [dataSource, setDataSource] = useState(listDataSource);
  const [topicQuestion, setTopicQuestion] = useState();

  const orgList = useRef([]);
  const listRef = useRef();

  const { questionNo } = props;

  useEffect(() => {
    setLoading(true);
    fetchList(pageNum);
    /* eslint-disable-next-line */
  },[pageNum]);

  // 获取回答列表接口调用
  const fetchList = pageNum => {
    const params = {
      pageNo: pageNum,
      pageSize,
      questionNo: questionNo,
      employeeId: window.$gVar.employeeId
    };
    api.getAnswerList(params).then(resData => {
      if(resData) {
        setTotalAnswerCounts(resData.totalAnswerCounts);
        setTopicQuestion(resData.topicQuestion);
        if(resData.answerInfo.length < 1){
          setLoading(false)
          return 
        }
        if(pageNum === 1){
          let arr = tranList(resData);
          setDataSource(dataSource.cloneWithRows(arr));
          orgList.current = arr;
        }else{
          setDataSource(dataSource.cloneWithRows([...orgList.current,...resData.answerInfo]));
          orgList.current = [...orgList.current,...resData.answerInfo];
        }
        setLoading(false)
      }
    });
    props.sendLog();
  }

  const tranList = content => {
    if(content.bestAnswer.length !== 0){
      content.bestAnswer[0].bestAnswerTag = "1";
      content.answerInfo[0].questionTag = "0"
    }
    const contentArr = content.bestAnswer.concat(content.answerInfo);
    return contentArr;
  }

  const onEndReached = () =>{
    setPageNum(pageNum + 1);
  }

  const onRefresh = () => {
    setPageNum(prev => {
      if (prev === 1) {
        fetchList(1);
      }
      return 1;
    });
  }

  const clickLike = (index, row) => {
    const params = {
      questionNo: questionNo,
	    answerNo: row.answerNo,
      employeeId: window.$gVar.employeeId
    };
    if(row.ifLike !== 1){
      api.updateLikeCounts(params).then(resData => {
        row.ifLike = 1;
        row.likeCounts += 1
        setDataSource(dataSource.cloneWithRows(dataSource._dataBlob.s1));
      })
    }
    props.sendLog();
  }

  const addAnswers = (answerText, hide, callback) => {
    Toast.loading('正在提交', 0);
    const requestParams = {
      hide,
      answerText,
      questionNo,
      employeeId: window.$gVar.employeeId
    }
    api.addAnswer(requestParams).then(res => {
      Toast.hide();
      if (res && res.answerCounts) {
        props.showMsg(`您是第${res.answerCounts}位智多星！`);
        onRefresh();
        callback();
        listRef.current.scrollTo(0, 0);   // 滚动至列表顶部
      }
    });
    props.sendLog();
  }

  const renderRow = (item, sectionID, rowID) => {
    return (
      <li className="answer-item" key={item.answerNo}>
        <div className="answerTag">
          {
            item.bestAnswerTag === "1" 
            ? "最赞回答" 
            : item.questionTag === "0" ? "全部回答" : null
          }
        </div>
        <div className="content">
          <img src={answerIcon} alt="" style={{width:'30px',height:'30px'}}/>
          <div className="right-content">
            <div>
              {
                item.hide === 1
                ? "匿名用户" + item.displayId
                : item.displayId
              }
            </div>
            <div className="answer-text">{item.answer}</div>
            <div
              className="like-img"
              onClick={() => clickLike(rowID,item)}
            >
              <img src={item.ifLike === 1 ? likedIcon : likeIcon} alt="" />
              <span>{item.likeCounts}</span>
            </div>
          </div>
        </div>
      </li>
    )
  }

  return(
    <div className="topic-square">
      <div className="header">
        <Icon
          className="crossIcon"
          type="left"
          size="lg"
          onClick={props.goTopics}
        />
        我要回答
      </div>
      <div className="subTitle">
        已经有{totalAnswerCounts}人回答
      </div>

      <div className="topic-square-bg">
        <ListView
          ref={listRef}
          className="answer-list"
          dataSource={dataSource}
          renderHeader={() => (
            <div className="topicTitle">
              {topicQuestion}
            </div>
          )}
          renderFooter={() => (
            <div style={{ padding: 10, textAlign: 'center' }}>
              {loading
                ? '加载中...'
                : orgList.current.length===0
                  ? "暂无回答"
                  : orgList.current.length > 8
                    ? '没有更多回答了'
                    : ''
              }
            </div>
          )}
          renderRow={renderRow}
          initialListSize={pageSize}
          pageSize={pageSize}
          scrollRenderAheadDistance={500}
          onEndReached={onEndReached} 
          onEndReachedThreshold={10}
        />

        <Speak
          isWXReady={props.isWXReady}
          subtitle={props.curSubtitle}
          onStart={props.onStart || (() => {})}
          onEnded={props.onEnded || (() => {})}
          onSubmit={addAnswers}
          sendLog={props.sendLog}
        />
      </div>
    </div>
  );
}
