import React, { useState, useEffect, useRef } from 'react';
import { Modal, Button, Toast, Icon } from 'antd-mobile';

import recorder from 'hooks/recorder';
import CssFade from 'components/CSSFade';
import Checkbox from 'components/CheckBox';
import Yinbo from './Yinbo';

import KeyboardSvg from 'static/speak/keyboard_light.svg';
import SendLightSvg from 'static/speak/sound_light.svg';
import SendSvg from 'static/speak/send.svg';
import MicrophoneSvg from 'static/speak/microphone.svg';
import DoubleArrowSvg from 'static/speak/close.svg';

import './index.scss';

let startRecordTime = 0;

export default props => {
  const [isMediaError, setIsMediaError] = useState(props.isWXReady ? false : true); // 是微信的话就不用判断是否开启录音失败
  const [isRecord, setIsRecord] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [isAnoymous, setIsAnoymous] = useState(false);
  const [asrLoading, setAsrLoading] = useState(false);
  const [recording, setRecording] = useState(false);

  const inputRef = useRef();
  const editRef = useRef();

  const onCreateMediaSuccess = () => {
    setIsMediaError(false);
  }

  const onCreateMediaFail = () => {
    setIsMediaError(true);
  }

  const handleRecordEndBusiness = () => {
    setRecording(false);
    if (new Date().getTime() - startRecordTime > 200) {
      setEditModalVisible(true);
      setAsrLoading(true);
      props.onEnded && props.onEnded();
    } else {
      props.onEnded && props.onEnded(false);
    }
  }

  const { startRecord, stopRecord } = recorder(props.isWXReady)(
    onCreateMediaSuccess,
    onCreateMediaFail,
    handleRecordEndBusiness   // 微信录音超过1分钟会自动停止，此方法处理停止录音后的业务逻辑
  );

  useEffect(() => {
    const savedMode = localStorage.getItem('dh-speak-mode');
    setIsRecord(savedMode === 'true');
    return () => {
      startRecordTime = null;
    }
  }, []);

  const onClose = () => {
    props.onClose && props.onClose();
    props.sendLog();
  }

  const onSubmit = () => {
    const submitContent = isRecord
      ? (editRef.current && editRef.current.innerText)
      : (inputRef.current && inputRef.current.innerText);

    console.log('submitContent: ', submitContent);
    if (!submitContent) {
      Toast.fail('提交内容不能为空');
      return;
    }

    const callback = () => {
      setEditModalVisible(false);
      if (inputRef.current) {
        inputRef.current.innerText = '';
      }
    }
    const isHide = isAnoymous ? 1 : 0;
    props.onSubmit(submitContent, isHide, callback);
  }

  const updateMode = () => {
    // 用户发言模式
    localStorage.setItem('dh-speak-mode', !isRecord);
    setIsRecord(!isRecord);
    props.sendLog();
  }

  const onRecordStart = e => {
    e.preventDefault();
    startRecord();
    setRecording(true);
    props.onStart && props.onStart();
    startRecordTime = new Date().getTime();
    props.sendLog();
  }

  const onRecordEnd = () => {
    stopRecord();
    handleRecordEndBusiness();
  }

  const onAnonymousChange = checked => {
    setIsAnoymous(checked);
    props.sendLog();
  }

  const onEditModalClose = () => {
    setEditModalVisible(false);
    props.sendLog();
  }

  useEffect(() => {
    if (!(props.subtitle && editRef.current)) {
      return;
    }
    setAsrLoading(true);
    setTimeout(() => {
      setAsrLoading(false);
    }, 2000);
    editRef.current.innerText = props.subtitle;
  }, [props.subtitle]);

  return (
    <>
      <div className="speak-wrap">
        <div className="speak">
          <div className="form-wrap">
            <span className="circle-icon" onClick={updateMode}>
              <img
                alt=""
                width="100%"
                height="100%"
                src={isRecord ? KeyboardSvg : SendLightSvg}
              />
            </span>
            
            {isRecord
              ? isMediaError
                ? <div className="speak-input call-recorder">
                    正在调起麦克风...
                  </div>
                : <div
                    className={`speak-input record-btn ${recording ? 'recording-btn' : ''}`}
                    onTouchStart={onRecordStart}
                    onTouchEnd={onRecordEnd}
                  >
                    按住 说话
                  </div>
              : <div
                  className="speak-input"
                  contentEditable
                  ref={inputRef}
                />
            }
            <span className="circle-icon submit-icon" onClick={onSubmit}>
              <img alt="" width="100%" height="100%" src={SendSvg} />
            </span>
          </div>
          <div className="anoymous-wrap">
            <Checkbox onChange={onAnonymousChange}>
              匿名发言
            </Checkbox>
          </div>
          
          {props.showClose && (
            <div className="speak-hide" onClick={onClose}>
              <img src={DoubleArrowSvg} alt="" />
            </div>
          )}
        </div>
      </div>

      <CssFade visible={recording}>
        <div className="speak-recording">
          <img src={MicrophoneSvg} alt="" />
          <Yinbo />
        </div>
      </CssFade>

      <Modal
        transparent
        className="sup-modal edit-modal"
        visible={editModalVisible}
        maskClosable={false}
        onClose={onEditModalClose}
      >
        <div className="title">录音识别结果编辑</div>
        <div ref={editRef} className="content" contentEditable />
        <CssFade visible={asrLoading}>
          <div className="asr-loading">
            <Icon type="loading" />
          </div>
        </CssFade>
        <div className="footer">
          <Button className="btn cus-btn bg-while" onClick={onEditModalClose}>
            取消
          </Button>
          <Button className="btn cus-btn" onClick={onSubmit}>
            提交
          </Button>
        </div>
      </Modal>
    </>
  );
}
