import React, { useState, useEffect, useRef } from 'react';

export default props => {
  const [cur, setCur] = useState(5);
  const list = useRef(new Array(5).fill(' '));

  useEffect(() => {
    let loop = setInterval(() => {
      setCur(prev => prev < 0 ? 5 : prev - 1);
    }, 300);

    return () => {
      clearInterval(loop);
      loop = null;
    }
  }, []);

  return (
    <ul className="yinbo">
      {list.current.map((item, i) => (
        <li
          key={i}
          className={i < cur ? '' : 'active'}
          style={{width: 10 * (5 - i)}}
        />
      ))}
    </ul>
  );
}