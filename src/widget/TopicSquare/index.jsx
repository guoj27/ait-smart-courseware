import React, { useState } from 'react';
import { Modal, Toast } from 'antd-mobile';
import dayjs from "dayjs";

import CssFade from 'components/CSSFade';
import Guide from './Guide';
import Topics from './Topics';
import Answers from './Answers';

import './index.scss';
import { useEffect } from 'react';
import {getUrlParam, reload} from 'util';

export default props => {
  const [guideVisible, setGuideVisible] = useState(false);
  const [topicsVisible, setTopicsVisible] = useState(false);
  const [answersVisible, setAnswersVisible] = useState(false);
  const [questionNo, setQuestionNo] = useState('');

  const { data = {} } = props.widget || {};

  useEffect(() => {
    if(getUrlParam().token){
      sessionStorage.setItem("token",  getUrlParam().token);
      window.ismini = true
    }else{
      window.ismini = false
    }

    if (data.guideType) {
      setGuideVisible(true);
    } else {
      setTopicsVisible(true);
    }
  }, [data.guideType]);

  const showMsg = text => {
    const title = '';
    const actions = [];
    const message = (
      <div className="speak-message-modal">
        {text}
      </div>
    );
    const alertModal = Modal.alert(title, message, actions);
    setTimeout(() => {
      alertModal.close();
    }, 2000);
  }

  const sendLog = () => {
    window.sendData({
      version: '1.0',
      action_type: 'log',
      sync: true,
      extra: {
        EventType: 'Topic_Square_Action',
        WebsocketID: window.$gVar.sessionId,
        dialog_node_id: window.$gVar.dialog_node_id,
        RequestID: window.$gVar.requestId,
        ClientTimeStamp: dayjs().format('YYYY-MM-DD HH:mm:ss.SSS'),
      }
    });
  }

  const hideAll = () => {
    setGuideVisible(false);
    setTopicsVisible(false);
    setAnswersVisible(false);
  }

  const goAnswers = questionNo => {
    setQuestionNo(questionNo);
    hideAll();
    setAnswersVisible(true);
  }

  const goTopics = () => {
    hideAll();
    setTopicsVisible(true);
  }

  return (
    <div className="topic-square-wrap">
      <CssFade visible={guideVisible}>
        <Guide
          { ...props }
          goTopics={goTopics}
          sendLog={sendLog}
        />
      </CssFade>
      <CssFade visible={topicsVisible}>
        <Topics
          { ...props }
          goAnswers={goAnswers}
          showMsg={showMsg}
          sendLog={sendLog}
        />
      </CssFade>
      <CssFade visible={answersVisible}>
        <Answers
          { ...props }
          questionNo={questionNo}
          goTopics={goTopics}
          showMsg={showMsg}
          sendLog={sendLog}
        />
      </CssFade>
    </div>
  );
};
