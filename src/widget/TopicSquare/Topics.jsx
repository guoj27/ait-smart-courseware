import React, {useEffect, useState, useRef} from 'react';
import { Icon, ListView, Toast } from "antd-mobile";

import CSSFade from 'components/CSSFade';
import Speak from "widget/TopicSquare/Speak";
import editIcon from 'static/icon/editIcon.png'

import { transTime } from 'util';
import api from 'axios/api';

const pageSize = 20;

export default props => {
  const [pageNum,setPageNum] = useState(1);
  const [loading, setLoading] = useState(true);
  const [totalQuestionCounts,setTotalQuestionCounts] = useState();
  const listDataSource  = new ListView.DataSource({
    rowHasChanged: (row1, row2) => row1 !== row2,
  });
  const [dataSource, setDataSource] = useState(listDataSource);
  const [speakVisible, setSpeakVisible] = useState(false);

  const orgList = useRef([]);
  const listRef = useRef();

  useEffect(() => {
    setLoading(true);
    fetchList(pageNum);
    /* eslint-disable-next-line */
  }, [pageNum]);

  // 接口调用
  const fetchList = pageNum => {
    let value = {
      pageNo: pageNum,
      pageSize
    };
    api.getTopicList(value).then(resData => {
      if(resData) {
        setTotalQuestionCounts(resData.totalQuestionCounts);

        if(resData.questionInfo.length<1){
          setLoading(false);
          return;
        }
        if(pageNum === 1){
          setDataSource(dataSource.cloneWithRows(resData.questionInfo));
            orgList.current = resData.questionInfo;
        }else{
          setDataSource(dataSource.cloneWithRows([...orgList.current,...resData.questionInfo]));
          orgList.current = [...orgList.current,...resData.questionInfo];
        }
        setLoading(false);
      }
    });
    props.sendLog();
  }

  const addTopic = (questionText, hide, callback) => {
    Toast.loading('正在提交', 0);
    const requestParams = {
      hide,
      questionText,
      employeeId: window.$gVar.employeeId,
      questionTime: new Date().getTime(),
    }
    api.addTopic(requestParams).then(res => {
      Toast.hide();
      if (res) {
        callback();
        res.similarCount && props.showMsg(`有${res.similarCount}人与你有相似的疑惑！`);
        setSpeakVisible(false);
        onRefresh();
        listRef.current.scrollTo(0, 0);   // 滚动至列表顶部
      }
    });
    props.sendLog();
  }

  const onRefresh = () => {
    setPageNum(prev => {
      if (prev === 1) {
        fetchList(1);
      }
      return 1;
    });
  }

  const onEndReached = () =>{
    setPageNum(pageNum + 1)
  }

  const edit = () => {
    setSpeakVisible(true);
    props.sendLog();
  }

  const back = () => {
    window.sendData({
      "version": "1.0",
      "action_type": "click",
      "text": "press_关闭话题广场"
    });
  }

  const topicLevel = count => {
    if(count === 0){
      return ("#f7e6bc");
    }else if( count <5){
      return ("#F4C447")
    }else if(count <10){
      return("#DF6633")
    }else{
      return("#9E1A19")
    }
  }

  const renderRow = (item, sectionID, rowID) => {
    return (
      <li 
        className="topic"
        key={item.questionNo}
        onClick={() => props.goAnswers(item.questionNo)}
      >
        <div className="itemTitle">
          <span 
            className="circle" 
            style={{backgroundColor:topicLevel(item.answerCounts)}}
          ></span>
          {item.question} 
          {
            item.answerCounts >= 10 ?
            <span className="smallIcon">HOT</span>
            :null
          }
          
        </div>
          <div className="subTitle">
            <span>{transTime(item.updateTime)}</span>
            <span>{item.answerCounts}人回答</span>
          </div>
      </li>
    );
  }

  return (
    <div className="topic-square">
      <div className="header">
        <Icon
          className="crossIcon"
          type="left"
          size="lg"
          onClick={back}
        />
        话题广场
      </div>
      <div className="subTitle">
        已经有{totalQuestionCounts}人提问
      </div>
      
      <div className="topic-square-bg">
        <ListView
          ref={listRef}
          className="topic-list"
          dataSource={dataSource}
          renderFooter={() => (
            <div style={{ padding: 10, textAlign: 'center' }}>
              {loading
                ? '加载中...'
                : orgList.current.length > 8
                  ? '没有更多话题了'
                  : '' 
              }
            </div>
          )}
          renderRow={renderRow}
          initialListSize={pageSize}
          pageSize={pageSize}
          scrollRenderAheadDistance={500}   //当一个行接近屏幕范围多少像素之内的时候，就开始渲染这一行
          onEndReached={onEndReached}       //上拉加载
          onEndReachedThreshold={10}        //调用onEndReached之前的临界值，单位是像素
        >
        </ListView>

        <CSSFade visible={!speakVisible}>
          <div className="edit" onClick={edit}>
            <img src={editIcon} alt=""/>
          </div>
        </CSSFade>

        <CSSFade visible={speakVisible}>
          <Speak
            showClose
            isWXReady={props.isWXReady}
            subtitle={props.curSubtitle}
            onStart={props.onStart || (() => {})}
            onEnded={props.onEnded || (() => {})}
            onSubmit={addTopic}
            onClose={() => setSpeakVisible(false)}
            sendLog={props.sendLog}
          />
        </CSSFade>
      </div>
    </div>
  );
}
