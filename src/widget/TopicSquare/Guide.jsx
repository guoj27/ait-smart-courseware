import React from "react";
import { Button } from "antd-mobile";

export default props => {
  const { data = {} } = props.widget || {};
  
  const redirect = () => {
    props.goTopics();
    props.sendLog();
  }

  return (
    <div className="topic-square">
      <div className="guide-cover">
        <div className="guide-img-wrap">
          <div className="guide-img-container">
            <img
              className="guide-img" 
              alt=""
              src={data.guideType ? require(`static/topics/${data.guideType}.png`) : ""}
            />
            <Button className="guide-btn" onClick={redirect}>
              前往话题广场
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
