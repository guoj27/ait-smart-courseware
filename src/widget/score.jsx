import React, { useEffect, useState } from "react";
import dayjs from "dayjs";
import { Button} from "antd-mobile";

let myChart = null;

export default props => {
  const { data = {} } = props.widget;
  const [thisProDe, setThisProDe] = useState(0);

  useEffect(() => {
    const yibiaoTimer = setTimeout(() => {
      setThisProDe(data.score * 1.8 ) ;
  }, 1000);
  return () => {
      clearTimeout(yibiaoTimer);
  };
  },[data.score])

  const goDocu = () => {
    window.sendData({
      "version": "1.0",
      "action_type": "click",
      "text": "press_资料库"
    });
  }

  const exit = () => {
    window.sendData({
      "version": "1.0",
      "action_type": "click",
      "text": "press_重新挑战"
    });
  }
    
  return (
    <div className="score-bg">
      <div className="content-bg">
        <div className="content-border" >
          <div className="top-bg" ></div>
          <div className="score-content">
              <img className="top-img" src={require('../static/icon/xujia.png')} alt=""></img>
              <div className="score-content-bg">
                <div className="top-content">
                  <div className="sub-title">您的得分</div>
                  {/* <div ref={chartRef} className="radar-chart" ></div> */}
                  <div className="dashboard-bg">
                    <div
                      className="pointer pointer-left"
                      style={{ transform: `rotate(${thisProDe}deg)` }}
                    />
                    <div className="score-total">{data.score}<span>%</span></div>
                  </div>
                  <div className="title">{data.title}</div>

                  <div className="botttom-content">
                    <div onClick={goDocu}>
                      <img src={require("static/icon/docu-btn.png")} alt=""/>
                    </div>
                  </div>
                  
                  <Button onClick={exit} className="exit">再玩一次</Button>
                </div>
              </div>
              
          </div>
        </div>
      </div>
    </div>
  );
};
