import React, { useState } from "react";

import { Button} from "antd-mobile";

const cdnSrc = "https://spdb.cdn.bcebos.com";
export default props => {
  
  const { data = {}, isTrue, standardChoice } = props.widget || {};
  const [isAutoPlay, setIsAutoPlay] = useState(false);
  const [selectNum, setSelectnum] = useState();
  const [isTemp, setIsTemp] = useState(true);
  const [showBtn, setShowBtn] = useState(false)

  const checkAnswer = (num, i) => {
      setSelectnum(num);
      setIsTemp(true);
      setShowBtn(true)
      
  }

  const next = () => {
    if(isTemp){
      setIsAutoPlay(true);
      setIsTemp(false);
      window.sendData({
        "version": "1.0",
        "action_type": "form",
        "text": "press_确认",
        "extra": {
          "form": {
            "global": {
              "AID": window.$gVar.AID,
              "answer": selectNum,
              "text": "press_确认",
            }
          }
        }
      });
      
    }else{
      setSelectnum("");
      setIsAutoPlay(false);
      setShowBtn(false);
      window.sendData({
        "version": "1.0",
        "action_type": "click",
        "text": "press_继续"
      });
    }
  }
 
  return (
    <div className="question-bg">
      <div className="question-title">
        <span className="dot" >{data.num}</span>
        <div className="question">{data.title}</div>
      </div>
      <div className="question-answer">
        {
          data.titlePicUrl ? 
          <img className="question-img" src={cdnSrc + data.titlePicUrl} alt=""/>
          : null
        }
        <ul style={{height: !data.titlePicUrl ? "80%" : "50%", pointerEvents: !isTemp ? "none" : "auto"}}>
          <audio 
            autoPlay={isAutoPlay}
            preload="auto"
            src={isAutoPlay ? (cdnSrc + "/AICourse001/audio/" + (isTrue === "" ? "" : (isTrue === "true" ? "success" : "fail" ) +".mp3")) : ""}
          ></audio>
          {data.picList && data.picList.map((item,i) => (
            <li key={i}  onClick={() => checkAnswer(item.num, i)}>
              {/* <span className={`num dot ${"select"+i}`}> */}
              <span 
                className={`num dot ${item.num === selectNum ? 
                  (isTrue === "" ? "" : (isTrue === "true" ? "success" : "fail shaky" )) : ""}
                  ${item.num === selectNum ? isTemp ? "tempState" : "" : ""}
                `}>
                {
                  item.num === selectNum ? "" : item.num
                }
              </span>
              {
                !item.rate ? 
                <img src={cdnSrc + item.picUrl} alt=""/>
                : <span>{item.rate}</span>
              }
            </li>
          ))}
        </ul>
        
        <div className="bottom-content" style={{display: showBtn ? "block" : "none"}}>
          <div className="tip" style={{color: isTrue ? "#1677FF" : "red"}}>
            {isTrue === "" ? "" : isTrue === "true" ? "恭喜你答对了！" : "很遗憾你答错啦，正确答案选" + standardChoice }
          </div>
            <Button className="next-btn" onClick = {next}>
              {isTemp ? "确认" : "继续"}
            </Button>
        </div>
      </div>
    </div>
  );
}
