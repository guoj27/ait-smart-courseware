import axios from 'axios';
import { Toast } from 'antd-mobile';
import { getBaseUrl } from 'util';

//过滤请求
axios.interceptors.request.use(
  config => {
    const token = sessionStorage.getItem("token");
    config.headers.Authorization = `${token}`;
    config.timeout = 30 * 1000;
    config.baseURL = getBaseUrl();
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    // console.log("axios response:", response);
    const data = response.data;
    if (data || data === true ||data === false) {
      return Promise.resolve(data);
    }
    return Promise.reject();
  },
  error => {
    console.log("axios error:", error);
    const { response } = error;
    // const status = error.status || (response ? response.status : 0);
    
    if(response.config.url === "/api/digitalhuman/aitrainer/loginWithCode" && response.data.status){
      return Toast.fail("输入错误或不在白名单内，请重试");
    }else if(response){
      const msg = `${response.data}`;
      return Toast.fail(msg);
    }

    const returnError =
      error.message
        ? error.message.includes("timeout")
          ? Error("接口调用超时")
          : error
        : Error("请求失败");
    return Promise.reject(returnError);
  }
);

export default {
  get: (url, data) => axios.get(url, {params: data}),
  post: (url, data, config) => axios.post(url, data, config),
  put: (url, data) => axios.put(url, data),
  delete: (url, data) => axios.delete(url, {params: data})
}
