import jsonp from 'jsonp';
import { Toast } from 'antd-mobile';

import axios from "./index";
import { getBaseUrl } from 'util';

let sseSource;

//登录
function devLogin(param) {
   return axios.post("/api/digitalhuman/aitrainer/login", param)
    .then(resData => {
      if (sseSource instanceof EventSource) {
        sseSource.close();
      }
      return resData;
    })
    .catch(err => {
      Toast.fail(err ? err.data : "请求失败");
      return null;
    });
}

function getCount(aid) {
  return axios.get(`/api/digitalhuman/aitrainer/count/${aid}`)
   .then(resData => {
       return resData;
   })
   .catch(err => {
     Toast.fail(err ? err.data : "请求失败");
     return null;
   });
}

function sseCount(aid) {
  if (typeof EventSource !== 'undefined') {
    return new Promise((resolve, reject) => {
      const requestUrl = `${getBaseUrl()}/api/digitalhuman/aitrainer/count-event/${aid}`;
      const source = new EventSource(requestUrl);
      source.onmessage = res => {
        resolve(JSON.parse(res.data || '{}'));
      };
      source.onerr = res => {
        reject(res);
      };
      if (sseSource instanceof EventSource) {
        sseSource.close();
      }
      sseSource = source;
    });
  } else {
    console.log("不支持EventSource");
    return getCount(aid);
  }
}

const getWXConfig = () => {
  const url = window.location.href.split('#')[0];

  let requestUrl = 'https://validation.energytrust.com.cn/v1/weixin/jssdk';
  requestUrl += `?url=${encodeURIComponent(url)}`;
  requestUrl += `&api=startRecord,stopRecord,onVoiceRecordEnd,uploadVoice,updateAppMessageShareData`;
  requestUrl += `&debug=true`;

  return new Promise((resolve, reject) => {
    jsonp(
      requestUrl,
      { timeout: 30 * 1000 },
      (err, data) => {
        console.log('jsonp callback: ', data);
        if (data) {
          resolve(data);
        } else {
          reject(err);
        }
      }
    );
  });
}

const getWXToken = () => {
  const url = window.location.href.split('#')[0];

  let requestUrl = 'https://validation.energytrust.com.cn/v1/weixin/token';
  requestUrl += `?url=${encodeURIComponent(url)}`;
  
  return new Promise((resolve, reject) => {
    jsonp(
      requestUrl,
      { timeout: 30 * 1000 },
      (err, data) => {
        console.log('jsonp callback: ', data);
        if (data) {
          resolve(data);
        } else {
          reject(err);
        }
      }
    );
  });
}

const getPufaWXConfig = () => {
    const url = window.location.href.split('#')[0];
    const requestUrl = `/api/digitalhuman/aitrainer/wx/getSignature?url=${encodeURIComponent(url)}`
    return axios.get(requestUrl)
        .then(res => {
            if (res) {
                return res;
            }
            throw Error();
        });
};

//获取话题list
const getTopicList = (params) => {
  return axios.post(`/api/digitalhuman/aitrainer/square/question/findAllQuestion`, params)
   .then(resData => {
       return resData;
   })
   .catch(err => {
     Toast.fail(err ? err.data : "请求失败");
     return null;
   });
}

//获取回答
const getAnswerList = (params) => {
  return axios.post(`/api/digitalhuman/aitrainer/square/question/findAllAnswer`, params) 
  .then(resData => {
    return resData;
  })
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
    return null;
  });
}

//点赞
const updateLikeCounts = params => {
  return axios.post(`/api/digitalhuman/aitrainer/square/question/updateLikeCounts`, params)
  .then(resData => {
    return resData;
  })
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
    return null;
  });
}

//新增话题
const addTopic = params => {
  return axios.post(`/api/digitalhuman/aitrainer/square/question/updateQuestion`, params)
  .then(resData => resData)
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
  });
}

//新增回答
const addAnswer = params => {
  return axios.post(`/api/digitalhuman/aitrainer/square/question/updateAnswer`, params)
  .then(resData => {
    return resData;
  })
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
    return null;
  });
}

//图形验证码
const getImgcode = () => {
  return axios.get("/api/digitalhuman/aitrainer/getImageCode")
  .then(resData => {
    return resData;
  })
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
    return null;
  });
}
//校验验证码
const checkCode = params => {
  return axios.post("/api/digitalhuman/aitrainer/checkCode",params)
  .then(resData => {
    return resData;
  })
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
    return null;
  });
}

const loginWithCode = params => {
  return axios.post("/api/digitalhuman/aitrainer/loginWithCode",params)
  .then(resData => {
    return resData;
  })
  .catch(err => {
    Toast.fail(err ? err.data : "请求失败");
    return null;
  }); 
}
export default {
  devLogin,
  getCount,
  sseCount,
  getWXConfig,
  getPufaWXConfig,
  getTopicList,
  getAnswerList,
  updateLikeCounts,
  addTopic,
  addAnswer,
  getImgcode,
  checkCode,
  loginWithCode,
  getWXToken
}
