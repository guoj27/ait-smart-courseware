import React from 'react';
import ReactDOM from 'react-dom';
import dayjs from 'dayjs'
import 'default-passive-events';
import VConsole from 'vconsole';

import * as serviceWorker from './serviceWorker';
import App from './App';

import 'animate.css/animate.min.css'
import './index.scss';

window.sendData = () => {return false}; // 注册空函数，防止调用错误
window.$gVar.cdnPath = process.env.REACT_APP_cdn_host + process.env.REACT_APP_cdn_video_path;

const urlParse = new URL(window.location.href);
const time = Date.parse(new Date()); 
window.$gVar.AID = urlParse.searchParams.get('AID') || 'AICourse001';  //AIBOSTraining001//AICourse001//AIMiniTraining009

let videoVersion = '';

if(window.$gVar.AID) {
  // 同步请求idle视频
  // const textUrl = `${process.env.REACT_APP_cdn_host}/${window.$gVar.AID}/idle-videos.txt?v=${time}`;
  // const videoVersionUrl = `${process.env.REACT_APP_cdn_host}/${window.$gVar.AID}/VERSION.txt?v=${time}`;

  try {
    const xhr = new XMLHttpRequest();

    // xhr.open('GET', textUrl, false);
    // xhr.send();
    // if (xhr.readyState === 4 && xhr.status === 200) {
    //   window.$gVar.idleVideos = xhr.response.split('\n').filter(el=>el) || [];
    // }

    // xhr.open('GET', videoVersionUrl, false);
    xhr.send();
    if (xhr.readyState === 4 && xhr.status === 200) {
      videoVersion = xhr.response;
    }
  } catch (e) {}
}

// 添加vconsole 和 版本号
const addDebug = () => {
  let clickTime = 0;
  let isVisible = false;
  let vc = null;

  // 版本号
  const versionInput = document.getElementById('version');
  const versionSpan = document.createElement('span');
  versionSpan.className = 'version-text';
  versionSpan.innerText = `${dayjs(parseInt(versionInput.value)).format('YYYY-MM-DD HH:mm:ss.SSS')}${videoVersion ? ' | '+videoVersion:''}`;
  document.body.appendChild(versionSpan);

  const debugBtn = document.createElement('button');
  debugBtn.className = 'm-vc-btn';
  debugBtn.addEventListener('touchstart', function(e) {
    e.preventDefault();
    clickTime ++;
    setTimeout(() => {
      clickTime = 0
    }, 400);

    // 双击
    if (clickTime >= 2) {
      isVisible = !isVisible;
      versionSpan.style.display = isVisible ? 'block' : 'none';
      // 生产上vconsole会消耗内存
      /* eslint-disable no-undef */
      // if (DEPLOY_ENV !== 'prod') {
        if (!vc) {
          vc = new VConsole();
        }
        if (vc.$dom) {
          vc.$dom.style.display = isVisible ? 'block' : 'none';
        }
      // }
    }
  });
  document.body.appendChild(debugBtn);
}
addDebug();

// if (DEPLOY_ENV !== 'dev') {
//   window.onbeforeunload = function() {
//     return "您确定要离开吗";
//   }
// }

ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
