const {
  override,
  fixBabelImports,
  addPostcssPlugins,
  setWebpackPublicPath,
  addWebpackPlugin,
  addWebpackAlias
} = require('customize-cra');
const path = require("path");

const FileManagerWebpackPlugin = require('filemanager-webpack-plugin');
const webpack = require('webpack');
const dayjs = require('dayjs');

const isProd = process.env.NODE_ENV === 'production';
let publicPath = '/fundation/';
if(process.argv[2]){
    publicPath = process.argv[2].replace('PUBLIC_URL=','')
}
let deployEnv = '';
if(process.argv[3]) {
    deployEnv = process.argv[3].replace('--env=','')
}
const NOW_TIME = dayjs().format('YYYYMMDDHHmm');
const zipFileName = `${deployEnv}__aiTrainingCourseware__${NOW_TIME}`;

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd-mobile',
        style: 'css',
    }),
    addPostcssPlugins([
        require('postcss-aspect-ratio-mini')({}),
        require('postcss-px-to-viewport')({
            viewportWidth: 750/2, // (Number) The width of the viewport.
            viewportHeight: 1334/2, // (Number) The height of the viewport.
            unitPrecision: 3, // (Number) The decimal numbers to allow the REM units to grow to.
            viewportUnit: 'vw', // (String) Expected units.
            exclude: [/node_modules/],
            selectorBlackList: ['.ignore', '.hairlines'], // (Array) The selectors to ignore and leave as px.
            minPixelValue: 1, // (Number) Set the minimum pixel value to replace.
            mediaQuery: false // (Boolean) Allow px to be converted in media queries.
        }),
        require('postcss-write-svg')({
            utf8: false
        })
    ]),
    isProd && addWebpackPlugin(
      new FileManagerWebpackPlugin ({
        onEnd: {
            archive: [
                {source: './build', destination: `./build_zip/${zipFileName}.zip`},
            ]
        }
      })
    ),
    addWebpackPlugin(
      new webpack.DefinePlugin({
        DEPLOY_ENV: isProd ? JSON.stringify(deployEnv) : JSON.stringify('dev'),
        PUBLIC_PATH: isProd ? JSON.stringify(publicPath) : JSON.stringify('/')
      })
    ),
    isProd && setWebpackPublicPath(publicPath),
    addWebpackAlias({
      ["util"]: path.resolve(__dirname, "src/util.js")
    }),
);
