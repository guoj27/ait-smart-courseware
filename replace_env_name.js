const fs = require('fs');
const envFileName1 = '.env.production';
let prodVersion = '';
for (let i = 0; i < process.argv.length; i++) {
  const argvElement = process.argv[i];
  if(argvElement.indexOf('--env=') !== -1) {
    prodVersion = argvElement.replace('--env=','')
  }
}
const envFileName2 = `.env.${prodVersion}`;

if(fs.existsSync(envFileName1)){
  fs.renameSync(envFileName1,envFileName2)
} else if(fs.existsSync(envFileName2)){
  fs.renameSync(envFileName2,envFileName1)
}
